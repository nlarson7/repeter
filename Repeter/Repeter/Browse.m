//
//  Browse.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "BaseViewController.h"
#import "Browse.h"
#import "B1Cell.h"
#import "B2Cell.h"
#import "YTS.h"
#import "YTObject.h"
#import "PLObject.h"
#import "CurrentPlaylist.h"
#import "LocalStorage.h"

@interface Browse ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, YTSDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) UIView *searchView;
@property (nonatomic) UITextField *searchText;
@property (nonatomic) NSArray *searchResults;
@property (nonatomic) YTObject *selectedObject;
@property (nonatomic) BOOL hasSearched;

@property (nonatomic) UIButton *topTitle;

@end

@implementation Browse

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    
    self.title = @"BROWSE";
    self.tabBarItem.image = [UIImage imageNamed:@"music_note"];
    self.hasSearched = NO;
    
//    [[YTS search] loadTopCharts];
    return self;
}

- (void)viewDidLoad
{
    self.topTitle = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.topTitle.frame = CGRectMake(0, 0, 150, 30);
    
    if ([[LocalStorage storage] loadLastSearchTerm] == nil) {
        [self.topTitle setTitle:@"Popular" forState:UIControlStateNormal];
    }else {
        [self.topTitle setTitle:[[LocalStorage storage] loadLastSearchTerm] forState:UIControlStateNormal];
    }
    
    
    [self.topTitle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.topTitle.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:22.0]];
    self.navigationItem.titleView = self.topTitle;
    
//    UILabel *titleLabel = [UILabel new];
//    titleLabel.text = @"All Genres";
//    titleLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:22.0];
//    titleLabel.frame = CGRectMake(0, 0, 150, 30);
//    titleLabel.textAlignment = NSTextAlignmentCenter;
//    titleLabel.textColor = [UIColor whiteColor];
//    self.navigationItem.titleView = titleLabel;
    
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonTapped)];
    self.navigationItem.leftBarButtonItem = searchButton;
    
    self.navigationController.navigationBarHidden = NO;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [[UIColor clearColor] setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:image
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0];
    
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(0.0, 64.0, self.view.bounds.size.width, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2];
    [self.view addSubview:seperatorLine];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 64.0, self.view.bounds.size.width, self.view.bounds.size.height - 64.0 - 50.0) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([B1Cell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([B1Cell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([B2Cell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([B2Cell class])];
    [self.view addSubview:self.tableView];
    
    self.tableView.estimatedRowHeight = 160.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.searchView = [UIView new];
    self.searchView.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 44.0);
    self.searchView.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:1.0];
    self.searchView.alpha = 0.0;
    [self.navigationController.navigationBar addSubview:self.searchView];
    
    self.searchText = [UITextField new];
    self.searchText.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 96.0, 44.0);
    self.searchText.font = [UIFont fontWithName:@"AvenirNext-Regular" size:18.0];
    self.searchText.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:1.0];
    self.searchText.textColor = [UIColor whiteColor];
    self.searchText.returnKeyType = UIReturnKeySearch;
    self.searchText.keyboardAppearance = UIKeyboardAppearanceDark;
    UIColor *color = [UIColor colorWithWhite:200.0 / 255.0 alpha:1.0];
    self.searchText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search YouTube" attributes:@{NSForegroundColorAttributeName: color}];
    self.searchText.delegate = self;
    [self.searchView addSubview:self.searchText];
    
    UIButton *doneSearchingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneSearchingButton.frame = CGRectMake(self.view.bounds.size.width - 44.0, 0.0, 44.0, 44.0);
    [doneSearchingButton setTitle:@"X" forState:UIControlStateNormal];
    [doneSearchingButton.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:22]];
    [doneSearchingButton setTitleColor:[UIColor colorWithWhite:180.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneSearchingButton addTarget:self action:@selector(doneSearchingButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.searchView addSubview:doneSearchingButton];
    
    [self setupVideoOptions];
    
    if (!self.hasSearched) {
        
        if ([[LocalStorage storage] loadLastSearch] == nil) {
            [YTS search].delegate = self;
            [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Loading Videos.."];
            [[YTS search] searchForVideoWithKeyWords:@"BYU Noteworthy BYU VocalPoint Pentatonix"];
        }else {
            self.searchResults = [NSArray arrayWithArray:[[LocalStorage storage] loadLastSearch]];
            [self.tableView reloadData];
        }
        
        
    }
}

- (void)searchButtonTapped
{
    [self.searchText becomeFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.searchView.alpha = 1.0;
    }];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.hasSearched = YES;
    [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Loading Videos.."];
    
    [YTS search].delegate = self;
    [[LocalStorage storage] saveLastSearchTerm:textField.text];
    [[YTS search] searchForVideoWithKeyWords:textField.text];
    [textField resignFirstResponder];
    return YES;
}

- (void)YTSSearchSuccessfulWithResults:(NSArray *)results
{
    [self.topTitle setTitle:[[LocalStorage storage] loadLastSearchTerm] forState:UIControlStateNormal];
    
    self.searchResults = [NSArray arrayWithArray:results];
    [self.tableView reloadData];
    
    [((BaseViewController *)self.tabBarController) hideLoadingView];
}

- (void)doneSearchingButtonTapped
{
    [self.searchText resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.searchView.alpha = 0.0;
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row % 2 == 0) {
        B1Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([B1Cell class]) forIndexPath:indexPath];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell setupCellWithObject:[self.searchResults objectAtIndex:indexPath.row]];
        
        [[cell viewWithTag:536] removeFromSuperview];
        
        if (indexPath.row > 0) {
            UIView *seperatorLine = [UIView new];
            seperatorLine.tag = 536;
            seperatorLine.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 32.0, 0.5);
            seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
            [cell addSubview:seperatorLine];
        }
        
        cell.addToPlaylistButton = ^(id sender) {
            self.selectedObject = [self.searchResults objectAtIndex:indexPath.row];
            [self showVideoOptionsForYTObject:self.selectedObject];
        };
        
        cell.playVideoButton = ^(id sender) {
            
            int valSelected = (int)indexPath.row;
            
            NSMutableArray *mArray = [NSMutableArray new];
            for (int i = valSelected; i < self.searchResults.count; i++) {
                [mArray addObject:self.searchResults[i]];
            }
            for (int i = 0; i < valSelected; i++) {
                [mArray addObject:self.searchResults[i]];
            }
            
            PLObject *pl = [PLObject PLObjectWithPlaylistId:nil playlistName:self.searchText.text videos:[NSArray arrayWithArray:mArray]];
            YTObject *video = (YTObject *)[self.searchResults objectAtIndex:indexPath.row];
            
            [[CurrentPlaylist playlist] playVideo:video fromPlaylist:pl];
            
            [((BaseViewController *)self.tabBarController) setSelectedIndex:2];
            
        };
        
        return cell;
    }else {
        B2Cell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([B2Cell class]) forIndexPath:indexPath];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell setupCellWithObject:[self.searchResults objectAtIndex:indexPath.row]];
        
        [[cell viewWithTag:536] removeFromSuperview];
        
        if (indexPath.row > 0) {
            UIView *seperatorLine = [UIView new];
            seperatorLine.tag = 536;
            seperatorLine.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 32.0, 0.5);
            seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
            [cell addSubview:seperatorLine];
        }
        
        cell.addToPlaylistButton = ^(id sender) {
            self.selectedObject = [self.searchResults objectAtIndex:indexPath.row];
            [self showVideoOptionsForYTObject:self.selectedObject];
        };
        
        cell.playVideoButton = ^(id sender) {
            
            int valSelected = (int)indexPath.row;
            
            NSMutableArray *mArray = [NSMutableArray new];
            for (int i = valSelected; i < self.searchResults.count; i++) {
                [mArray addObject:self.searchResults[i]];
            }
            for (int i = 0; i < valSelected; i++) {
                [mArray addObject:self.searchResults[i]];
            }
            
            PLObject *pl = [PLObject PLObjectWithPlaylistId:nil playlistName:self.searchText.text videos:[NSArray arrayWithArray:mArray]];
            YTObject *video = (YTObject *)[self.searchResults objectAtIndex:indexPath.row];
            
            [[CurrentPlaylist playlist] playVideo:video fromPlaylist:pl];
            
            [((BaseViewController *)self.tabBarController) setSelectedIndex:1];
        };
        
        return cell;
    }
    
    
}

- (void)setupVideoOptions
{
    

}

- (void)showVideoOptionsForYTObject:(YTObject *)object
{
    [((BaseViewController *)self.tabBarController) showVideoOptionsForYTObject:object];
}

@end
