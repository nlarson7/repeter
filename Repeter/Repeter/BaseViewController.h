//
//  BaseViewController.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTObject.h"

@protocol AddedToPlaylistDelegate <NSObject>

- (void)didAddVideoToPlaylist;
- (void)didCancelAddingVideoToPlaylist;

@end

@interface BaseViewController : UITabBarController

@property (nonatomic, assign) id<AddedToPlaylistDelegate>addingToPlaylistDelegate;

- (void)addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage;

- (void)showVideoOptionsForYTObject:(YTObject *)object;

- (void)showLoadingViewWithText:(NSString *)text;
- (void)hideLoadingView;
- (BOOL)isShowingLoadingView;

- (void)showGenreOptions;
- (void)hideGenreOptions;

@end
