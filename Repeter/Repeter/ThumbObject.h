//
//  ThumbObject.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThumbObject : NSObject

@property (nonatomic) NSString *thumbnailUrl;
@property (nonatomic) UIImage *thumbnailImage;

- (id)initWithThumbnailUrl:(NSString *)thumbnailUrl thumbnailImage:(UIImage *)thumbnailImage;

+ (id)ThumbObjectWithThumbnailUrl:(NSString *)thumbnailUrl thumbnailImage:(UIImage *)thumbnailImage;

@end
