//
//  LocalStorage.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "LocalStorage.h"
#import "Server.h"

@interface LocalStorage ()<ServerLoadPlaylists>

@property (nonatomic) NSArray *localThumbObjects;
@property (nonatomic) NSArray *localPlaylists;

@end

@implementation LocalStorage

+ (LocalStorage *)storage
{
    static LocalStorage *storage = nil;
    
    if (!storage) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            storage = [[self alloc] initPrivate];
        });
    }
    return storage;
}

- (instancetype)initPrivate
{
    self = [super init];
    [self loadThumbObjects];
    return self;
}

- (NSArray *)thumbObjects
{
    return self.localThumbObjects;
}

- (void)saveThumbObject:(ThumbObject *)object
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"thumb_objects"];
    
    NSString *encodedString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    NSMutableArray *mArray = [NSMutableArray new];
    
    if (encodedString != nil) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:encodedString options:0];
        NSArray *objects = [NSArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        
        mArray = [NSMutableArray arrayWithArray:objects];
    }
    
    [mArray addObject:object];
    
    NSArray *savedArray = [NSArray arrayWithArray:mArray];
    NSData *dataToStore = [NSKeyedArchiver archivedDataWithRootObject:savedArray];
    NSString *stringToStore = [dataToStore base64EncodedStringWithOptions:0];
    
    if ([stringToStore writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil]) {
        [self loadThumbObjects];
    }
}

- (void)loadThumbObjects
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"thumb_objects"];
    
    NSString *encodedString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    if (encodedString != nil) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:encodedString options:0];
        NSArray *objects = [NSArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        
        self.localThumbObjects = [NSArray arrayWithArray:objects];
    }
}

#pragma mark - Playlist Storage

- (void)updatePlaylists
{
    [Server API].serverLoadedPlaylistsDelegate = self;
    [[Server API] sendData:nil toEndPoint:@"LOAD_PLAYLISTS"];
}

- (void)loadedPlaylists:(NSArray *)playlists
{
    [self savePlaylists:playlists];
}

- (void)savePlaylists:(NSArray *)playlists
{
    // Write to file.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist_objects"];
    
    NSData *dataToStore = [NSKeyedArchiver archivedDataWithRootObject:playlists];
    NSString *stringToStore = [dataToStore base64EncodedStringWithOptions:0];
    
    if ([stringToStore writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil]) {
        [self playlistsAreSaved];
    }

}

- (void)playlistsAreSaved
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"playlist_objects"];
    
    NSString *encodedString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    if (encodedString != nil) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:encodedString options:0];
        NSArray *objects = [NSArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        
        self.localPlaylists = [NSArray arrayWithArray:objects];
        
        if ([self.playlistLocalStorageDelegate respondsToSelector:@selector(playlistsUpdated)]) {
            [self.playlistLocalStorageDelegate playlistsUpdated];
        }
        
    }
}

- (NSArray *)loadPlaylists
{
    if (self.localPlaylists == nil || self.localPlaylists.count == 0) {
        [self updatePlaylists];
    }
    
    return self.localPlaylists;
}


- (void)saveLastSearch:(NSArray *)lastSearch
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"search_objects"];
    
    NSData *dataToStore = [NSKeyedArchiver archivedDataWithRootObject:lastSearch];
    NSString *stringToStore = [dataToStore base64EncodedStringWithOptions:0];
    
    if ([stringToStore writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil]) {
        // Great.
    }
}

- (NSArray *)loadLastSearch
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"search_objects"];
    
    NSString *encodedString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    if (encodedString != nil) {
        NSData *data = [[NSData alloc] initWithBase64EncodedString:encodedString options:0];
        NSArray *objects = [NSArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
        
        return [NSArray arrayWithArray:objects];
    }else {
        return nil;
    }
}

- (void)saveLastSearchTerm:(NSString *)lastSearchTerm
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"search_term"];
    
    [lastSearchTerm writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

- (NSString *)loadLastSearchTerm
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"search_term"];
    
    NSString *encodedString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    if (encodedString != nil) {
        return encodedString;
    }else {
        return nil;
    }
}

@end
