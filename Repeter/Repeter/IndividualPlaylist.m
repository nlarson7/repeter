//
//  IndividualPlaylist.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "IndividualPlaylist.h"
#import "PlaylistCell.h"
#import "Server.h"
#import "BaseViewController.h"
#import "CurrentPlaylist.h"

@interface IndividualPlaylist ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UILabel *titleLabel;

@end

@implementation IndividualPlaylist

- (void)viewDidLoad
{
    self.titleLabel = [UILabel new];
    self.titleLabel.text = self.playlist.playlistName;
    self.titleLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:22.0];
    self.titleLabel.frame = CGRectMake(0, 0, 150, 30);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = self.titleLabel;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [[UIColor clearColor] setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:image
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0];
    
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(0.0, 64.0, self.view.bounds.size.width, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2];
    [self.view addSubview:seperatorLine];
    
    self.scrollView = [UIScrollView new];
    self.scrollView.frame = CGRectMake(0.0, 64.0, self.view.bounds.size.width, 60.0);
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width * 2.0, 60.0);
    [self.view addSubview:self.scrollView];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 124.0, self.view.bounds.size.width, self.view.bounds.size.height - 64.0 - 50.0 - 60.0) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PlaylistCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PlaylistCell class])];
    [self.view addSubview:self.tableView];
    
    [self loadPlaylistsForUser];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.playlist.videos.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlaylistCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlaylistCell class]) forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell setupCellWithYTObject:[self.playlist.videos objectAtIndex:indexPath.row]];
    
    
    [[cell viewWithTag:536] removeFromSuperview];
    
    if (indexPath.row > 0) {
        UIView *seperatorLine = [UIView new];
        seperatorLine.tag = 536;
        seperatorLine.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 32.0, 0.5);
        seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
        [cell addSubview:seperatorLine];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        YTObject *obj = (YTObject *)[self.playlist.videos objectAtIndex:indexPath.row];
        
        NSMutableArray *mArray = [NSMutableArray arrayWithArray:self.playlist.videos];
        [mArray removeObject:obj];
        
        self.playlist.videos = [NSArray arrayWithArray:mArray];
        
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        [[Server API] sendData:@{@"unique_id":obj.unique_id} toEndPoint:@"DELETE_VIDEO"];
        
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSNumber *item = (self.playlist.videos)[(NSUInteger)indexPath.row];
    NSLog(@"Tapped view number: %@", item);
    
    int valSelected = (int)indexPath.row;
    
    NSMutableArray *mArray = [NSMutableArray new];
    for (int i = valSelected; i < self.playlist.videos.count; i++) {
        [mArray addObject:self.playlist.videos[i]];
    }
    for (int i = 0; i < valSelected; i++) {
        [mArray addObject:self.playlist.videos[i]];
    }
    PLObject *pl = [PLObject PLObjectWithPlaylistId:nil playlistName:nil videos:[NSArray arrayWithArray:mArray]];
    YTObject *video = (YTObject *)[self.playlist.videos objectAtIndex:indexPath.row];
    
    
    
    [[CurrentPlaylist playlist] playVideo:video fromPlaylist:pl];
    
    
    
    
    [((BaseViewController *)self.tabBarController) setSelectedIndex:1];
}

- (void)loadPlaylistsForUser
{
    self.scrollView.contentSize = CGSizeMake(180.0 * self.allPlaylists.count, 60.0);
    
    float xPos = 0.0;
    for (PLObject *playlist in self.allPlaylists) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(xPos, 0.0, 160.0, 60.0);
        [button setTitle:playlist.playlistName forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.3] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-DemiBold" size:18.0]];
        [button addTarget:self action:@selector(changedPlaylist:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:button];
        xPos = xPos + 160.0;
        
        if ([playlist.playlistName isEqualToString:self.playlist.playlistName]) {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    
}

- (void)changedPlaylist:(UIButton *)sender
{
    for (int i = 0; i < self.scrollView.subviews.count; i++) {
        
        if ([self.scrollView.subviews[i] isKindOfClass:[UIButton class]]) {
            [(UIButton *)self.scrollView.subviews[i] setTitleColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.3] forState:UIControlStateNormal];
        }
    }
    
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    for (PLObject *plObj in self.allPlaylists) {
        if ([plObj.playlistName isEqualToString:sender.titleLabel.text]) {
            self.playlist = plObj;
            self.titleLabel.text = plObj.playlistName;
        }
    }
    [self.tableView reloadData];
}


@end
