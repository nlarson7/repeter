//
//  B1Cell.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "B1Cell.h"
#import "LocalStorage.h"
#import "ThumbObject.h"
#import "PLObject.h"
#import "YTObject.h"

@interface B1Cell ()


@end

@implementation B1Cell

- (void)setupCellWithObject:(YTObject *)object
{
    self.infoView.layer.cornerRadius = 3.0;
    self.infoView.layer.shadowColor = [UIColor colorWithWhite:25.0 / 255.0 alpha:0.8].CGColor;
    self.infoView.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.infoView.layer.shadowRadius = 1.5;
    self.infoView.layer.shadowOpacity = 0.5;
    
    self.playView.layer.cornerRadius = 30.0;
    self.playView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.playView.layer.borderWidth = 0.5;
    
    [self.playView addTarget:self action:@selector(tappedPlay:) forControlEvents:UIControlEventTouchUpInside];
    
    self.videoCover.layer.cornerRadius = 3.0;
    self.videoCover.layer.shadowColor = [UIColor colorWithWhite:25.0 / 255.0 alpha:0.8].CGColor;
    self.videoCover.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.videoCover.layer.shadowRadius = 1.5;
    self.videoCover.layer.shadowOpacity = 0.5;
    
    self.videoImage.layer.cornerRadius = 3.0;
    self.videoImage.layer.shadowColor = [UIColor colorWithWhite:25.0 / 255.0 alpha:0.8].CGColor;
    self.videoImage.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.videoImage.layer.shadowRadius = 1.5;
    self.videoImage.layer.shadowOpacity = 0.5;
    
    BOOL isOnPlaylist = NO;
    
    NSArray *playlists = [[LocalStorage storage] loadPlaylists];
    
    for (PLObject *play in playlists) {
        for (YTObject *video in play.videos) {
            if ([video.videoId isEqualToString:object.videoId]) {
                isOnPlaylist = YES;
                break;
            }
        }
    }
    
    if (isOnPlaylist) {
        [self.likeOutlet setImage:[[UIImage imageNamed:@"openHeart2filled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self.likeOutlet.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        [self.likeOutlet setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
    }else {
        [self.likeOutlet setImage:[[UIImage imageNamed:@"openHeart2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self.likeOutlet.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        [self.likeOutlet setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
    }

    
    [self.videoTitle sizeToFit];
    
    [self.likeOutlet addTarget:self action:@selector(tappedOptions:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.videoTitle.text = object.videoTitle;
    self.channelTitle.text = object.channelTitle;
    
    // View count
    int curCount = object.viewCount.intValue;
    
    if (curCount > 999999999) {
        int viewC = curCount / 1000000000;
        self.viewCount.text = [NSString stringWithFormat:@"%iB views", viewC];
    }else if (curCount > 999999) {
        int viewC = curCount / 1000000;
        self.viewCount.text = [NSString stringWithFormat:@"%iM views", viewC];
    }else if (curCount > 999) {
        int viewC = curCount / 1000;
        self.viewCount.text = [NSString stringWithFormat:@"%iK views", viewC];
    }else {
        self.viewCount.text = [NSString stringWithFormat:@"%i views", curCount];
    }
    
    // Video Image, check to see if it's stored locally or not.
    // If so, use it, if not don't - and use the URL as the key for the image.
    
    self.videoImage.image = nil;
    
    NSArray *storedThumbs = [NSArray arrayWithArray:[[LocalStorage storage] thumbObjects]];
    
    BOOL stored = NO;
    for (ThumbObject *thumb in storedThumbs) {
        if ([thumb.thumbnailUrl isEqualToString:object.thumbNailUrl]) {
            self.videoImage.image = thumb.thumbnailImage;
            stored = YES;
            break;
        }
    }
    
    if (!stored) {
        dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(aQueue, ^{
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.thumbNailUrl]]];
            
            [[LocalStorage storage] saveThumbObject:[ThumbObject ThumbObjectWithThumbnailUrl:object.thumbNailUrl thumbnailImage:image]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.videoImage.image = image;
            });
            
        });
        
    }

}

- (void)tappedPlay:(UIButton *)sender
{
    self.playVideoButton(sender);
}

- (void)tappedOptions:(UIButton *)sender
{
    
    self.addToPlaylistButton(sender);
    
}

@end
