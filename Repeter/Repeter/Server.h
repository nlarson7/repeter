//
//  Server.h
//  Repeter
//
//  Created by Nathan Larson on 6/29/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServerAddedVideoToPlaylistDelegate <NSObject>
- (void)videoAddedToPlaylist;
@end

@protocol ServerCreatePlaylist <NSObject>
- (void)playlistCreated;
@end

@protocol ServerLoadPlaylists <NSObject>
- (void)loadedPlaylists:(NSArray *)playlists;
@end

@protocol ServerCreateUserDelegate <NSObject>
- (void)userCreated;
@end

@interface Server : NSObject

+ (Server *)API;

- (void)sendData:(NSDictionary *)data toEndPoint:(NSString *)key;

@property (nonatomic, assign) id<ServerCreateUserDelegate>serverCreateUserDelegate;
@property (nonatomic, assign) id<ServerLoadPlaylists>serverLoadedPlaylistsDelegate;
@property (nonatomic, assign) id<ServerCreatePlaylist>serverCreatePlaylistDelegate;
@property (nonatomic, assign) id<ServerAddedVideoToPlaylistDelegate>serverAddedVideoToPlaylistDelegate;

@end
