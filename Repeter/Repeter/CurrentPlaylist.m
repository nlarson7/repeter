//
//  CurrentPlaylist.m
//  Repeter
//
//  Created by Nathan Larson on 6/30/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "CurrentPlaylist.h"

@interface CurrentPlaylist ()

@property (nonatomic) PLObject *currentPlaylistObject;
@property (nonatomic) YTObject *currentVideoObject;

@end

@implementation CurrentPlaylist

+ (CurrentPlaylist *)playlist
{
    static CurrentPlaylist *playlist = nil;
    
    if (!playlist) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            playlist = [[self alloc] initPrivate];
        });
    }
    return playlist;
}

- (instancetype)initPrivate
{
    self = [super init];
    return self;
}

- (void)playVideo:(YTObject *)video fromPlaylist:(PLObject *)playlist_object
{
    self.currentVideoObject = video;
    self.currentPlaylistObject = playlist_object;
    
    NSNotification * notification = [[ NSNotification alloc]
                                     initWithName:@"playVideo" object:nil userInfo:@{@"video":video
                                                                                     ,@"playlist":playlist_object}];
    
    [[NSNotificationCenter defaultCenter] postNotification:notification];
    
}

- (void)playNextVideo
{
    NSMutableArray *mArray = [NSMutableArray arrayWithArray:self.currentPlaylistObject.videos];
    YTObject *firstObject = (YTObject *)mArray[0];
    [mArray removeObjectAtIndex:0];
    [mArray addObject:firstObject];
    self.currentPlaylistObject.videos = [NSArray arrayWithArray:mArray];
    self.currentVideoObject = (YTObject *)self.currentPlaylistObject.videos[0];
    
    NSNotification * notification = [[ NSNotification alloc]
                                     initWithName:@"playVideo" object:nil userInfo:@{@"video":self.currentVideoObject
                                                                                     ,@"playlist":self.currentPlaylistObject}];
    
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

- (void)playPreviousVideo
{
    NSMutableArray *mArray = [NSMutableArray arrayWithArray:self.currentPlaylistObject.videos];
    YTObject *firstObject = (YTObject *)[mArray lastObject];
    [mArray removeLastObject];
    [mArray insertObject:firstObject atIndex:0];
    self.currentPlaylistObject.videos = [NSArray arrayWithArray:mArray];
    self.currentVideoObject = (YTObject *)self.currentPlaylistObject.videos[0];
    
    NSNotification * notification = [[ NSNotification alloc]
                                     initWithName:@"playVideo" object:nil userInfo:@{@"video":self.currentVideoObject
                                                                                     ,@"playlist":self.currentPlaylistObject}];
    
    [[NSNotificationCenter defaultCenter] postNotification:notification];
}

@end
