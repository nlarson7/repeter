//
//  ViewController.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "ViewController.h"

#import "YTPlayerView.h"

@interface ViewController ()<YTPlayerViewDelegate>
@property (weak, nonatomic) IBOutlet YTPlayerView *playerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    
    NSString *videoId = @"M7lc1UVf-VE";
    
    // For a full list of player parameters, see the documentation for the HTML5 player
    // at: https://developers.google.com/youtube/player_parameters?playerVersion=HTML5
    NSDictionary *playerVars = @{
                                 @"controls" : @1,
                                 @"playsinline" : @1,
                                 @"autohide" : @1,
                                 @"showinfo" : @0,
                                 @"modestbranding" : @1,
                                 @"autoplay" : @1
                                 };
    self.playerView.delegate = self;
    [self.playerView loadWithVideoId:videoId playerVars:playerVars];
    
    
}

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView
{
//    [playerView playVideo];
}

- (void)playerView:(YTPlayerView *)playerView didPlayTime:(float)playTime
{
    NSLog(@"\nCurrent Time: %f\n", playTime);
    
    if (playTime > 15.0) {
        [self.playerView seekToSeconds:5.0 allowSeekAhead:YES];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
//    [self.playerView playVideo];
//
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
