//
//  YTS.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol YTSDelegate <NSObject>

- (void)YTSSearchSuccessfulWithResults:(NSArray *)results;

@end

@interface YTS : NSObject

+ (YTS *)search;

@property (nonatomic, assign) id<YTSDelegate>delegate;

- (void)searchForVideoWithKeyWords:(NSString *)keywords;

- (void)loadTopCharts;

- (void)loadVideoCategories;

@end
