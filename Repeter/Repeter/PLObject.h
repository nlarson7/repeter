//
//  PLObject.h
//  Repeter
//
//  Created by Nathan Larson on 6/29/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTObject.h"

@interface PLObject : NSObject

@property (nonatomic) NSString *playlistId;
@property (nonatomic) NSString *playlistName;
@property (nonatomic) NSArray *videos;

- (id)initWithPlaylistId:(NSString *)playlist_id playlistName:(NSString *)playlist_name videos:(NSArray *)videos;

+ (id)PLObjectWithPlaylistId:(NSString *)playlist_id playlistName:(NSString *)playlist_name videos:(NSArray *)videos;

- (void)addVideo:(YTObject *)video;

- (void)removeVideo:(YTObject *)video;

@end
