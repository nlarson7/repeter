//
//  PlaylistCell.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PLObject.h"
#import "YTObject.h"

@interface PlaylistCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *playButtonOutlet;
@property (weak, nonatomic) IBOutlet UILabel *itemTitle;
@property (weak, nonatomic) IBOutlet UILabel *itemDescription;
@property (weak, nonatomic) IBOutlet UILabel *itemPlayTime;

@property (nonatomic, copy) void (^playPlaylist)(id sender);

- (void)setupCellWithPLObject:(PLObject *)object;
- (void)setupCellWithYTObject:(YTObject *)object;

@end
