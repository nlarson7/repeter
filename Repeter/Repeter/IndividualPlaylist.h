//
//  IndividualPlaylist.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PLObject.h"
#import "YTObject.h"

@interface IndividualPlaylist : UIViewController

@property (nonatomic) PLObject *playlist;
@property (nonatomic) NSArray *allPlaylists;

@end
