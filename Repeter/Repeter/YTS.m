//
//  YTS.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "YTS.h"
#import "YTObject.h"
#import "LocalStorage.h"

@interface YTS ()

@property (nonatomic) NSString *googleAPIKEY;

@end

@implementation YTS

+ (YTS *)search
{
    static YTS *search = nil;
    
    if (!search) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            search = [[self alloc] initPrivate];
        });
    }
    return search;
}

- (instancetype)initPrivate
{
    self = [super init];
    self.googleAPIKEY = @"AIzaSyCCRVfdR_GdoTAk0R8EwleCv79LVtdvcOw";
    return self;
}

- (void)searchForVideoWithKeyWords:(NSString *)keywords
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue, ^{
        
        NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/search?part=snippet&q=\%@&type=\%@&key=\%@&maxResults=20", keywords, @"video", self.googleAPIKEY];
        
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *getURL = [NSURL URLWithString:urlString];
        
        NSData *returnData = [NSData dataWithContentsOfURL:getURL];
        
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableContainers error:nil];
        NSArray *array = [dictionary valueForKey:@"items"];
        
        NSMutableArray *searchResults = [NSMutableArray new];
        
        NSString *builderString = nil;
        NSURL *builderUrl = nil;
        NSData *builderData = nil;
        NSString *viewCount = nil;
        NSString *fullTimeString = nil;
        NSString *hours = nil;
        NSString *minutes = nil;
        NSString *seconds = nil;
        
        for (NSDictionary *result in array) {
            
            // Go get the statistics as well.
            builderString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=statistics,contentDetails&id=%@&key=%@",[[result objectForKey:@"id"] valueForKey:@"videoId"], self.googleAPIKEY];
            builderUrl = [NSURL URLWithString:builderString];
            builderData = [NSData dataWithContentsOfURL:builderUrl];
            NSDictionary *builderDictionary = [NSJSONSerialization JSONObjectWithData:builderData options:NSJSONReadingMutableContainers error:nil];
            
            viewCount = [[[[builderDictionary objectForKey:@"items"] objectAtIndex:0] objectForKey:@"statistics"] valueForKey:@"viewCount"];
            
            fullTimeString = [[[[builderDictionary objectForKey:@"items"] objectAtIndex:0] objectForKey:@"contentDetails"] valueForKey:@"duration"];
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:@"PT" withString:@""];
            
            NSRange range1 = [fullTimeString rangeOfString:@"H"];
            if (range1.location == NSNotFound) {
                hours = @"0";
            }else {
                hours = [fullTimeString substringToIndex:range1.location];
            }
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@H", hours] withString:@""];
            
            NSRange range2 = [fullTimeString rangeOfString:@"M"];
            if (range2.location == NSNotFound) {
                minutes = @"0";
            } else {
                minutes = [fullTimeString substringToIndex:range2.location];
            }
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@M", minutes] withString:@""];
            
            NSRange range3 = [fullTimeString rangeOfString:@"S"];
            if (range3.location == NSNotFound) {
                seconds = @"0";
            }else {
                seconds = [fullTimeString substringToIndex:range3.location];
            }
            
            YTObject *object = [YTObject YTObjectWithVideoId:[[result objectForKey:@"id"] valueForKey:@"videoId"] videoTitle:[[result objectForKey:@"snippet"] valueForKey:@"title"] videoDescription:[[result objectForKey:@"snippet"] valueForKey:@"description"] channelTitle:[[result objectForKey:@"snippet"] valueForKey:@"channelTitle"] viewCount:viewCount thumbNailUrl:[[[[result objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"high"] valueForKey:@"url"] playlistId:nil hours:hours minutes:minutes seconds:seconds unique_id:nil];
            
            [searchResults addObject:object];
        }
        
        [[LocalStorage storage] saveLastSearch:searchResults];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(YTSSearchSuccessfulWithResults:)]) {
                [self.delegate YTSSearchSuccessfulWithResults:searchResults];
            }
        });
        
    });
    
}

- (void)loadTopCharts
{
    dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(aQueue, ^{
        
        NSString *urlString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&chart=mostPopular&regionCode=US&maxResults=15&key=%@", self.googleAPIKEY];
        
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *getURL = [NSURL URLWithString:urlString];
        
        NSData *returnData = [NSData dataWithContentsOfURL:getURL];
        
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableContainers error:nil];
        NSArray *array = [dictionary valueForKey:@"items"];
        
        NSMutableArray *searchResults = [NSMutableArray new];
        
        NSString *builderString = nil;
        NSURL *builderUrl = nil;
        NSData *builderData = nil;
        NSString *viewCount = nil;
        NSString *fullTimeString = nil;
        NSString *hours = nil;
        NSString *minutes = nil;
        NSString *seconds = nil;
        
        for (NSDictionary *result in array) {
            
            // Go get the statistics as well.
            builderString = [NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=statistics,contentDetails&id=%@&key=%@",[result valueForKey:@"id"], self.googleAPIKEY];
            builderUrl = [NSURL URLWithString:builderString];
            builderData = [NSData dataWithContentsOfURL:builderUrl];
            NSDictionary *builderDictionary = [NSJSONSerialization JSONObjectWithData:builderData options:NSJSONReadingMutableContainers error:nil];
            
            viewCount = [[[[builderDictionary objectForKey:@"items"] objectAtIndex:0] objectForKey:@"statistics"] valueForKey:@"viewCount"];
            
            fullTimeString = [[[[builderDictionary objectForKey:@"items"] objectAtIndex:0] objectForKey:@"contentDetails"] valueForKey:@"duration"];
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:@"PT" withString:@""];
            
            NSRange range1 = [fullTimeString rangeOfString:@"H"];
            if (range1.location == NSNotFound) {
                hours = @"0";
            }else {
                hours = [fullTimeString substringToIndex:range1.location];
            }
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@H", hours] withString:@""];
            
            NSRange range2 = [fullTimeString rangeOfString:@"M"];
            if (range2.location == NSNotFound) {
                minutes = @"0";
            } else {
                minutes = [fullTimeString substringToIndex:range2.location];
            }
            
            fullTimeString = [fullTimeString stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@M", minutes] withString:@""];
            
            NSRange range3 = [fullTimeString rangeOfString:@"S"];
            if (range3.location == NSNotFound) {
                seconds = @"0";
            }else {
                seconds = [fullTimeString substringToIndex:range3.location];
            }
            
            YTObject *object = [YTObject YTObjectWithVideoId:[result objectForKey:@"id"] videoTitle:[[result objectForKey:@"snippet"] valueForKey:@"title"] videoDescription:[[result objectForKey:@"snippet"] valueForKey:@"description"] channelTitle:[[result objectForKey:@"snippet"] valueForKey:@"channelTitle"] viewCount:viewCount thumbNailUrl:[[[[result objectForKey:@"snippet"] objectForKey:@"thumbnails"] objectForKey:@"high"] valueForKey:@"url"] playlistId:nil hours:hours minutes:minutes seconds:seconds unique_id:nil];
            
            [searchResults addObject:object];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([self.delegate respondsToSelector:@selector(YTSSearchSuccessfulWithResults:)]) {
                [self.delegate YTSSearchSuccessfulWithResults:searchResults];
            }
        });
        
    });

}




//GET https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&key={YOUR_API_KEY}

// For Statistics
// GET https://www.googleapis.com/youtube/v3/videos?part=statistics&id=Q5mHPo2yDG8&key={YOUR_API_KEY}


@end
