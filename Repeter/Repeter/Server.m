//
//  Server.m
//  Repeter
//
//  Created by Nathan Larson on 6/29/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "Server.h"
#import "PLObject.h"

@interface Server ()

@property (nonatomic) NSString *verification;
@property (nonatomic) NSDictionary *endpoints;

@end

@implementation Server

+ (Server *)API
{
    static Server *API = nil;
    
    if (!API) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            API = [[self alloc] initPrivate];
        });
    }
    return API;
}

- (instancetype)initPrivate
{
    self = [super init];
    
    self.verification = @"iefMksUIuchfu2834y823e9hWLdfj283r9jLs3dlLIjrhqS6FA4fYu5DFAfhP4dfRo48Fzzh2j3kfDuGu0iwlF32kdjf2";
    
    self.endpoints = @{@"CREATE_USER":@"CreateUser.php"
                       , @"LOAD_PLAYLISTS":@"LoadPlaylists.php"
                       , @"CREATE_PLAYLIST":@"CreatePlaylist.php"
                       , @"UPDATE_PLAYLIST":@"UpdatePlaylist.php"
                       , @"DELETE_PLAYLIST":@"DeletePlaylist.php"
                       , @"ADD_TO_PLAYLIST":@"AddToPlaylist.php"
                       , @"DELETE_VIDEO":@"DeleteVideoFromPlaylist.php"
                       , @"APP_ACTIVE":@"AppActive.php"};
    
    return self;
}

- (void)sendData:(NSDictionary *)data toEndPoint:(NSString *)key
{
    NSString *post = [NSString stringWithFormat:@"verify=%@&user_id=%@&end_point_key=%@", self.verification, [self retrieveUserId], key];
    
    for (NSString *key in data.allKeys) {
        post = [post stringByAppendingString:[NSString stringWithFormat:@"&%@=%@", key, [data valueForKey:key]]];
    }
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.appselevated.com/Repeter/API/%@", [self.endpoints valueForKey:key]]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data,NSURLResponse *response,NSError *error){
        
        if (data != nil && data.length != 0) {
            NSString *aString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            if (array.count > 0) {
                
                [self handleServerAPIResponse:array];
                
            }else {
                // A no data returned
            }
        }else {
            // a problem probably occured
        }
    }];
    
    [task resume];
}

- (void)handleServerAPIResponse:(NSArray *)response
{
    if ([[response valueForKey:@"end_point_key"] isEqualToString:@"CREATE_USER"]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:[response valueForKey:@"user_id"] forKey:@"user_id"];
        
        if ([self.serverCreateUserDelegate respondsToSelector:@selector(userCreated)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.serverCreateUserDelegate userCreated];
            });
        }
    }else if ([[response valueForKey:@"end_point_key"] isEqualToString:@"LOAD_PLAYLISTS"]) {
        
        // Create PLObjects
        NSMutableArray *playlists = [NSMutableArray new];
        for (NSDictionary *raw_obj in [response valueForKey:@"items"]) {
            
            NSArray *pls = [NSArray arrayWithArray:[raw_obj valueForKey:@"playlists"]];
            NSMutableArray *playlist_items = [NSMutableArray new];
            
            for (NSDictionary *ytObject in pls) {
                
                [playlist_items addObject:[YTObject YTObjectWithVideoId:[ytObject valueForKey:@"video_id"] videoTitle:[ytObject valueForKey:@"video_title"] videoDescription:[ytObject valueForKey:@"video_description"] channelTitle:[ytObject valueForKey:@"channel_title"] viewCount:[ytObject valueForKey:@"view_count"] thumbNailUrl:[ytObject valueForKey:@"thumbnail_url"] playlistId:[ytObject valueForKey:@"playlist_id"] hours:[ytObject valueForKey:@"hours"] minutes:[ytObject valueForKey:@"minutes"] seconds:[ytObject valueForKey:@"seconds"] unique_id:[ytObject valueForKey:@"id"]]];
                
            }
            
            PLObject *object = [PLObject PLObjectWithPlaylistId:[raw_obj valueForKey:@"playlist_id"] playlistName:[raw_obj valueForKey:@"playlist_name"] videos:[NSArray arrayWithArray:playlist_items]];
            [playlists addObject:object];
        }
        
        if ([self.serverLoadedPlaylistsDelegate respondsToSelector:@selector(loadedPlaylists:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.serverLoadedPlaylistsDelegate loadedPlaylists:playlists];
            });
        }
    }else if ([[response valueForKey:@"end_point_key"] isEqualToString:@"CREATE_PLAYLIST"] || [[response valueForKey:@"end_point_key"] isEqualToString:@"UPDATE_PLAYLIST"] || [[response valueForKey:@"end_point_key"] isEqualToString:@"DELETE_PLAYLIST"]) {
        if ([self.serverCreatePlaylistDelegate respondsToSelector:@selector(playlistCreated)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.serverCreatePlaylistDelegate playlistCreated];
            });
        }
    }else if ([[response valueForKey:@"end_point_key"] isEqualToString:@"ADD_TO_PLAYLIST"]) {
        if ([self.serverAddedVideoToPlaylistDelegate respondsToSelector:@selector(videoAddedToPlaylist)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.serverAddedVideoToPlaylistDelegate videoAddedToPlaylist];
            });
        }
    }else if ([[response valueForKey:@"end_point_key"] isEqualToString:@"APP_ACTIVE"]) {
        
    }
    
    NSLog(@"%@", response);
    
}



- (NSString *)retrieveUserId
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"];
}

@end
