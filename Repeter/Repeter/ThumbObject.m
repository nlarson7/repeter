//
//  ThumbObject.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "ThumbObject.h"

@implementation ThumbObject

- (id)initWithThumbnailUrl:(NSString *)thumbnailUrl thumbnailImage:(UIImage *)thumbnailImage
{
    self = [super init];
    if (self) {
        self.thumbnailUrl = thumbnailUrl;
        self.thumbnailImage = thumbnailImage;
    }
    return self;
}

+ (id)ThumbObjectWithThumbnailUrl:(NSString *)thumbnailUrl thumbnailImage:(UIImage *)thumbnailImage
{
    return [[self alloc] initWithThumbnailUrl:thumbnailUrl thumbnailImage:thumbnailImage];
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.thumbnailUrl forKey:@"thumbnailUrl"];
    [encoder encodeObject:self.thumbnailImage forKey:@"thumbnailImage"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.thumbnailUrl = [decoder decodeObjectForKey:@"thumbnailUrl"];
        self.thumbnailImage = [decoder decodeObjectForKey:@"thumbnailImage"];
    }
    return self;
}

@end
