//
//  PlaylistCell.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "PlaylistCell.h"

@interface PlaylistCell ()

@property (nonatomic) BOOL playlist;

@end

@implementation PlaylistCell

- (void)setupCellWithPLObject:(PLObject *)object
{
    self.playlist = YES;
    
    self.itemTitle.text = object.playlistName;
    
    if (object.videos.count == 1) {
        self.itemDescription.text = @"1 Video";
    }else {
        self.itemDescription.text = [NSString stringWithFormat:@"%i Videos", (int)object.videos.count];
    }
    
    int hours = 0;
    int minutes = 0;
    int seconds = 0;
    
    for (YTObject *ytObject in object.videos) {
        hours = hours + ytObject.hours.intValue;
        minutes = minutes + ytObject.minutes.intValue;
        seconds = seconds + ytObject.seconds.intValue;
    }
    
    int totalTime = hours * 60 * 60;
    totalTime = totalTime + minutes * 60;
    totalTime = totalTime + seconds;
    
    hours = totalTime / 3600;
    minutes = (totalTime % 3600) / 60;
    seconds = totalTime - (hours * 60 * 60) - (minutes * 60);
    
    NSString *finishedDuration = @"";
    
    if (hours > 0) {
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%ih ", hours]];
    }
    if (minutes > 0) {
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%im ", minutes]];
    }
    if (seconds > 0) {
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%is", seconds]];
    }
    
    self.itemPlayTime.text = finishedDuration;
    
    [self addPlayButtonFeatures];
}

- (void)setupCellWithYTObject:(YTObject *)object
{
    self.playlist = NO;
    
    self.itemTitle.text = object.videoTitle;
    self.itemDescription.text = object.channelTitle;
    
    NSString *hours = @"00";
    NSString *minutes = @"00";
    NSString *seconds = @"00";
    
    NSString *finishedDuration = @"";
    
    if (object.hours.intValue > 0) {
        if (object.hours.length == 1) {
            hours = [NSString stringWithFormat:@"0%@", object.hours];
        }else {
            hours = object.hours;
        }
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%@:", hours]];
    }
    
    if (object.minutes.intValue > 0) {
        if (object.minutes.length == 1) {
            minutes = [NSString stringWithFormat:@"0%@", object.minutes];
        }else {
            minutes = object.minutes;
        }
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%@:", minutes]];
    }else {
        finishedDuration = [finishedDuration stringByAppendingString:@"00:"];
    }
    
    if (object.seconds.intValue > 1) {
        if (object.seconds.length == 1) {
            seconds = [NSString stringWithFormat:@"0%@", object.seconds];
        }else {
            seconds = object.seconds;
        }
        finishedDuration = [finishedDuration stringByAppendingString:[NSString stringWithFormat:@"%@", seconds]];
    }else {
        finishedDuration = [finishedDuration stringByAppendingString:@"00"];
    }
    
    self.itemPlayTime.text = finishedDuration;
    
    [self addPlayButtonFeatures];
}

- (void)addPlayButtonFeatures
{
    self.playButtonOutlet.layer.cornerRadius = 20.0;
    self.playButtonOutlet.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.3].CGColor;
    self.playButtonOutlet.layer.borderWidth = 0.5;
}

- (IBAction)tappedPlay:(id)sender {
    if (self.playlist) {
        self.playPlaylist(sender);
    }
}



@end
