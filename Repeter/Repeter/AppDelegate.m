//
//  AppDelegate.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//
// Google API Key: AIzaSyDDe8pWcfJ4nYS4Qhw5h_mSfW9mfC6wfVg

#import "AppDelegate.h"
#import "LaunchS1.h"
#import "BaseViewController.h"
#import "Browse.h"
#import "Playlists.h"
#import "Playing.h"
#import "Discover.h"
#import "Profile.h"
#import "Server.h"
#import "LocalStorage.h"
#import <AVFoundation/AVFoundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import "SinglePlayer.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [Fabric with:@[[Twitter class]]];

    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    BOOL ok;
    NSError *setCategoryError = nil;
    ok = [audioSession setCategory:AVAudioSessionCategoryPlayback
                             error:&setCategoryError];
    if (!ok) {
        NSLog(@"%s setCategoryError=%@", __PRETTY_FUNCTION__, setCategoryError);
    }
    
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"] intValue] > 0) {
        
        Browse *browse = [Browse new];
        UINavigationController *bNav = [[UINavigationController alloc] initWithRootViewController:browse];
        
//        Discover *discover = [Discover new];
        Playing *playing = [Playing new];
        UINavigationController *playingNav = [[UINavigationController alloc] initWithRootViewController:playing];
        
        Playlists *playlists = [Playlists new];
        UINavigationController *playlistNav = [[UINavigationController alloc] initWithRootViewController:playlists];
        
//        Profile *profile = [Profile new];
        
        BaseViewController *tabbarController = [BaseViewController new];
        tabbarController.viewControllers = @[bNav, playingNav, playlistNav];
        
        [tabbarController addCenterButtonWithImage:nil highlightImage:nil];
        
        [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
        
        [[UITabBar appearance] setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        
        [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                            NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:8.0f]
                                                            } forState:UIControlStateNormal];
        self.window.rootViewController = tabbarController;
        
        [self.window makeKeyAndVisible];
    }else {
        LaunchS1 *viewController = [LaunchS1 new];
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        self.window.rootViewController = nav;
        
        [self.window makeKeyAndVisible];
    }
    
    
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
//    if ([[SinglePlayer player] isPlayerPlayingVideo]) {
        [[SinglePlayer player] continuePlayingVideo];
//    }
    
//    [self remoteControlReceivedWithEvent:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
    [[Server API] sendData:@{} toEndPoint:@"APP_ACTIVE"];
    [[LocalStorage storage] updatePlaylists];
    
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

//- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
//    
//    NSLog(@"Entered background");
//    
//    switch (event.subtype) {
//        case UIEventSubtypeRemoteControlPlay:
//        case UIEventSubtypeRemoteControlPause: // handle it break;
//        case UIEventSubtypeRemoteControlNextTrack: // handle it break;
//        case UIEventSubtypeRemoteControlPreviousTrack: // handle it break;
//        default: break;
//    }
//}

@end
