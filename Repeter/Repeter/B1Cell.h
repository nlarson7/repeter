//
//  B1Cell.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTObject.h"

@interface B1Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIButton *playView;
@property (weak, nonatomic) IBOutlet UIImageView *videoImage;
@property (weak, nonatomic) IBOutlet UIView *videoCover;
@property (weak, nonatomic) IBOutlet UIButton *likeOutlet;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *channelTitle;
@property (weak, nonatomic) IBOutlet UILabel *viewCount;

@property (nonatomic, copy) void (^addToPlaylistButton)(id sender);
@property (nonatomic, copy) void (^playVideoButton)(id sender);

- (void)setupCellWithObject:(YTObject *)object;

@end
