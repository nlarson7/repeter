//
//  Profile.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "Profile.h"

@implementation Profile

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    
    self.title = @"PROFILE";
    self.tabBarItem.image = [UIImage imageNamed:@"profile"];
    return self;
}

- (void)viewDidLoad
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    UILabel *comingSoon = [UILabel new];
    comingSoon.frame = CGRectMake(16.0, 16, self.view.bounds.size.width - 32, self.view.bounds.size.height - 32);
    comingSoon.textColor = [UIColor whiteColor];
    comingSoon.font = [UIFont fontWithName:@"AvenirNext-Regular" size:26.0];
    comingSoon.textAlignment = NSTextAlignmentCenter;
    comingSoon.text = @"Coming Soon";
    [self.view addSubview:comingSoon];
}

@end
