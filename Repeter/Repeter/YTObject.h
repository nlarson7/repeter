//
//  YTObject.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YTObject : NSObject

@property (nonatomic) NSString *unique_id;
@property (nonatomic) NSString *videoId;
@property (nonatomic) NSString *videoTitle;
@property (nonatomic) NSString *videoDescription;
@property (nonatomic) NSString *channelTitle;
@property (nonatomic) NSString *viewCount;
@property (nonatomic) NSString *thumbNailUrl;
@property (nonatomic) NSString *playlistId;
@property (nonatomic) NSString *hours;
@property (nonatomic) NSString *minutes;
@property (nonatomic) NSString *seconds;

- (id)initWithVideoId:(NSString *)videoId videoTitle:(NSString *)videoTitle videoDescription:(NSString *)videoDescription channelTitle:(NSString *)channelTitle viewCount:(NSString *)viewCount thumbNailUrl:(NSString *)thumbNailUrl playlistId:(NSString *)playlistId hours:(NSString *)hours minutes:(NSString *)minutes seconds:(NSString *)seconds unique_id:(NSString *)unique_id;

+ (id)YTObjectWithVideoId:(NSString *)videoId videoTitle:(NSString *)videoTitle videoDescription:(NSString *)videoDescription channelTitle:(NSString *)channelTitle viewCount:(NSString *)viewCount thumbNailUrl:(NSString *)thumbNailUrl playlistId:(NSString *)playlistId hours:(NSString *)hours minutes:(NSString *)minutes seconds:(NSString *)seconds unique_id:(NSString *)unique_id;

@end
