//
//  SinglePlayer.m
//  Repeter
//
//  Created by Nathan Larson on 7/3/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "SinglePlayer.h"

@interface SinglePlayer ()

@property (nonatomic) YTPlayerView *playerView;
@property (nonatomic) BOOL nowPlaying;

@end

@implementation SinglePlayer

+ (SinglePlayer *)player
{
    static SinglePlayer *player = nil;
    
    if (!player) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            player = [[self alloc] initPrivate];
        });
    }
    return player;
}

- (instancetype)initPrivate
{
    self = [super init];
    
    self.playerView = [YTPlayerView new];
    
    return self;
}

- (YTPlayerView *)getCurrentPlayerView
{
    return self.playerView;
}

- (void)playerIsPlaying:(BOOL)playing
{
    self.nowPlaying = playing;
}

- (void)continuePlayingVideo
{
    [self.playerView playVideo];
}

- (void)pausePlayingVideo
{
    [self.playerView pauseVideo];
}

- (BOOL)isPlayerPlayingVideo
{
    return self.nowPlaying;
}

@end
