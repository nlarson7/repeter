//
//  Playlists.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "Playlists.h"
#import "PlaylistCell.h"
#import "IndividualPlaylist.h"
#import "Server.h"
#import "PLObject.h"
#import "BaseViewController.h"
#import "CurrentPlaylist.h"
#import "LocalStorage.h"

@interface Playlists ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, PlaylistsUpdatedDelegate, ServerCreatePlaylist>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) UITextField *playlistName;
@property (nonatomic) UIView *addPlaylist;
@property (nonatomic) NSArray *localPlaylists;
@property (nonatomic) PLObject *editingObject;
@property (nonatomic) BOOL isEditingPlaylist;

@end

@implementation Playlists

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    
    self.title = @"PLAYLISTS";
    self.tabBarItem.image = [UIImage imageNamed:@"playlist"];
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = @"PLAYLISTS";
    [self loadPlaylists];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.navigationItem.title = @"";
}

- (void)viewDidLoad
{
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"Playlists";
    titleLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:22.0];
    titleLabel.frame = CGRectMake(0, 0, 150, 30);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [[UIColor clearColor] setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:image
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0];
    
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(0.0, 64.0, self.view.bounds.size.width, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2];
    [self.view addSubview:seperatorLine];

    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 64.0, self.view.bounds.size.width, self.view.bounds.size.height - 64.0 - 50.0) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PlaylistCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PlaylistCell class])];
    [self.view addSubview:self.tableView];
    
    UIBarButtonItem *addPlayList = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createNewPlaylist)];
    self.navigationItem.rightBarButtonItem = addPlayList;
    
    self.addPlaylist = [UIView new];
    self.addPlaylist.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 44.0);
    self.addPlaylist.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:1.0];
    self.addPlaylist.alpha = 0.0;
    [self.navigationController.navigationBar addSubview:self.addPlaylist];
    
    self.playlistName = [UITextField new];
    self.playlistName.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 96.0, 44.0);
    self.playlistName.font = [UIFont fontWithName:@"AvenirNext-Regular" size:18.0];
    self.playlistName.returnKeyType = UIReturnKeyGo;
    self.playlistName.textColor = [UIColor whiteColor];
    self.playlistName.keyboardAppearance = UIKeyboardAppearanceDark;
    UIColor *color = [UIColor colorWithWhite:200.0 / 255.0 alpha:1.0];
    self.playlistName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Playlist Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.playlistName.delegate = self;
    [self.addPlaylist addSubview:self.playlistName];
    
    UIButton *doneSearchingButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    doneSearchingButton.frame = CGRectMake(self.view.bounds.size.width - 44.0, 0.0, 44.0, 44.0);
    [doneSearchingButton setTitle:@"X" forState:UIControlStateNormal];
    [doneSearchingButton.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:22]];
    [doneSearchingButton setTitleColor:[UIColor colorWithWhite:180.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    [doneSearchingButton addTarget:self action:@selector(cancelAddPlaylistButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.addPlaylist addSubview:doneSearchingButton];
    
    self.isEditingPlaylist = NO;
    self.editingObject = nil;
}

- (void)createNewPlaylist
{
    [self.playlistName becomeFirstResponder];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.addPlaylist.alpha = 1.0;
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.addPlaylist.alpha = 0.0;
    }];
    
    if (self.playlistName.text.length == 0) {
        
    }else {
        // Show loading screen - fullscreen, from tabbarcontroller.
        
        // Send to server
        [Server API].serverCreatePlaylistDelegate = self;
        [LocalStorage storage].playlistLocalStorageDelegate = self;
        if (self.isEditingPlaylist) {
            
            [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Updating Playlist.."];
            
            [[Server API] sendData:@{@"playlist_name":self.playlistName.text
                                     , @"playlist_id":self.editingObject.playlistId} toEndPoint:@"UPDATE_PLAYLIST"];
        }else {
            
            [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Creating Playlist.."];
            
            [[Server API] sendData:@{@"playlist_name":self.playlistName.text} toEndPoint:@"CREATE_PLAYLIST"];
        }
    }
    
    return YES;
}

- (void)cancelAddPlaylistButtonTapped
{
    [self.playlistName resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.addPlaylist.alpha = 0.0;
    }];
}

- (void)playlistCreated
{
    [LocalStorage storage].playlistLocalStorageDelegate = self;
    [[LocalStorage storage] updatePlaylists];
}

- (void)playlistsUpdated
{
    [((BaseViewController *)self.tabBarController) hideLoadingView];
    [self loadPlaylists];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.localPlaylists.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 82.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlaylistCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlaylistCell class]) forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell setupCellWithPLObject:[self.localPlaylists objectAtIndex:indexPath.row]];
    
    [[cell viewWithTag:536] removeFromSuperview];
    
    if (indexPath.row > 0) {
        UIView *seperatorLine = [UIView new];
        seperatorLine.tag = 536;
        seperatorLine.frame = CGRectMake(16.0, 0.0, self.view.bounds.size.width - 32.0, 0.5);
        seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
        [cell addSubview:seperatorLine];
    }
    
    cell.playPlaylist = ^(id sender) {
        
        NSNumber *item = ((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).videos[(NSUInteger)indexPath.row];
        NSLog(@"Tapped view number: %@", item);
        
        int valSelected = (int)indexPath.row;
        
        NSMutableArray *mArray = [NSMutableArray new];
        for (int i = valSelected; i < ((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).videos.count; i++) {
            [mArray addObject:((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).videos[i]];
        }
        for (int i = 0; i < valSelected; i++) {
            [mArray addObject:((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).videos[i]];
        }
        PLObject *pl = [PLObject PLObjectWithPlaylistId:nil playlistName:nil videos:[NSArray arrayWithArray:mArray]];
        YTObject *video = (YTObject *)[((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).videos objectAtIndex:indexPath.row];

        
        [[CurrentPlaylist playlist] playVideo:video fromPlaylist:pl];
        
        [((BaseViewController *)self.tabBarController) setSelectedIndex:1];
    };
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IndividualPlaylist *playlist = [IndividualPlaylist new];
    playlist.playlist = [self.localPlaylists objectAtIndex:indexPath.row];
    playlist.allPlaylists = [NSArray arrayWithArray:self.localPlaylists];
    [self.navigationController pushViewController:playlist animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *modifyAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Edit Playlist" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        self.isEditingPlaylist = YES;
        self.editingObject = [self.localPlaylists objectAtIndex:indexPath.row];
        
        self.playlistName.text = ((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).playlistName;
        
        [self createNewPlaylist];
        
    }];
    modifyAction.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:1.0];
    
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Delete" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        
        [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Deleting Playlist.."];
        
        [Server API].serverCreatePlaylistDelegate = self;
        [[Server API] sendData:@{@"playlist_id":((PLObject *)[self.localPlaylists objectAtIndex:indexPath.row]).playlistId} toEndPoint:@"DELETE_PLAYLIST"];
        
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    
    return @[deleteAction, modifyAction];
}


- (void)loadPlaylists
{
    // Check if loading screen already present from tabbarcontroller.
    
//    if (![((BaseViewController *)self.tabBarController) isShowingLoadingView]) {
//        [((BaseViewController *)self.tabBarController) showLoadingViewWithText:@"Loading Playlists.."];
//    }
    
    [LocalStorage storage].playlistLocalStorageDelegate = self;
    self.localPlaylists = [[LocalStorage storage] loadPlaylists];
    [self.tableView reloadData];
}



@end
