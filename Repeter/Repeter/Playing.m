//
//  Playing.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "Playing.h"
#import "iCarousel.h"
#import "LocalStorage.h"
#import "ThumbObject.h"
#import "YTPlayerView.h"
#import "PLObject.h"
#import "YTObject.h"
#import "CurrentPlaylist.h"
#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SinglePlayer.h"
#import <MediaPlayer/MediaPlayer.h>

@interface Playing ()<iCarouselDataSource, iCarouselDelegate, YTPlayerViewDelegate, AddedToPlaylistDelegate>

@property (nonatomic) iCarousel *carousel;
@property (nonatomic) NSArray *playListItems;

@property (nonatomic) UILabel *currentSongTitle;
@property (nonatomic) UILabel *currentSongChannel;

@property (nonatomic) UIView *darkerVideoProgressTrack;
@property (nonatomic) UIView *actualVideoProgressTrack;
@property (nonatomic) UIView *videoProgressSlider;

@property (nonatomic) UIButton *playPauseButton;
@property (nonatomic) UIButton *nextVideoButton;
@property (nonatomic) UIButton *previousVideoButton;

@property (nonatomic) UIButton *repeatButton;
@property (nonatomic) UILabel *repeatText;

@property (nonatomic) YTPlayerView *playerView;
@property (nonatomic) YTObject *currentVideo;
@property (nonatomic) BOOL isPlaying;
@property (nonatomic) BOOL repeatSongEnabled;
@property (nonatomic) float totalSongSeconds;
@property (nonatomic) BOOL songIsOnPlaylist;

@property (nonatomic) UIButton *playListButton;

@end

@implementation Playing

- (id)init
{
    self = [super init];
    if (self) {
        
        self.isPlaying = NO;
        self.repeatSongEnabled = NO;
        self.playerView = [[SinglePlayer player] getCurrentPlayerView];
        [[SinglePlayer player] playerIsPlaying:NO];
        self.playerView.delegate = self;
        self.songIsOnPlaylist = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideoFromNotification:) name:@"playVideo" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    UILabel *titleLabel = [UILabel new];
    titleLabel.text = @"Now Playing";
    titleLabel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:22.0];
    titleLabel.frame = CGRectMake(0, 0, 150, 30);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = titleLabel;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    
    // create a 1 by 1 pixel context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [[UIColor clearColor] setFill];
    UIRectFill(rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.navigationController.navigationBar setBackgroundImage:image
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0];
    
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(0.0, 64.0, self.view.bounds.size.width, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2];
    [self.view addSubview:seperatorLine];
    
    self.currentSongTitle = [UILabel new];
    self.currentSongTitle.frame = CGRectMake(16.0, 80.0, self.view.bounds.size.width - 32.0, 44.0);
    self.currentSongTitle.textColor = [UIColor whiteColor];
    self.currentSongTitle.textAlignment = NSTextAlignmentCenter;
    self.currentSongTitle.font = [UIFont fontWithName:@"AvenirNext-Medium" size:18.0];
    self.currentSongTitle.text = @"";
    [self.view addSubview:self.currentSongTitle];
    
    self.currentSongChannel = [UILabel new];
    self.currentSongChannel.frame = CGRectMake(16.0, 105.0, self.view.bounds.size.width - 32.0, 44.0);
    self.currentSongChannel.textAlignment = NSTextAlignmentCenter;
    self.currentSongChannel.textColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.6];
    self.currentSongChannel.font = [UIFont fontWithName:@"AvenirNext-Regular" size:14.0];
    self.currentSongChannel.text = @"";
    [self.view addSubview:self.currentSongChannel];
    
    self.darkerVideoProgressTrack = [UIView new];
    self.darkerVideoProgressTrack.frame = CGRectMake(32.0, 170.0, self.view.bounds.size.width - 64.0, 10.0);
    self.darkerVideoProgressTrack.backgroundColor = [UIColor colorWithRed:140.0 / 255.0 green:18.0 / 255.0 blue:186.0 / 255.0 alpha:1.0];
    [self.view addSubview:self.darkerVideoProgressTrack];
    
    self.actualVideoProgressTrack = [UIView new];
    self.actualVideoProgressTrack.frame = CGRectMake(0.0, 0.0, 0.0, 10.0);
    self.actualVideoProgressTrack.backgroundColor = [UIColor colorWithRed:0.298 green:0.804 blue:0.384 alpha:1.00];
    [self.darkerVideoProgressTrack addSubview:self.actualVideoProgressTrack];
    
    self.videoProgressSlider = [UIView new];
    self.videoProgressSlider.frame = CGRectMake(0.0, 0.0, 6.0, 10.0);
    self.videoProgressSlider.backgroundColor = [UIColor whiteColor];
    
    UIView *smallSp = [UIView new];
    smallSp.frame = CGRectMake(0.0, 0.0, 1.0, 10.0);
    smallSp.backgroundColor = [UIColor colorWithRed:0.198 green:0.704 blue:0.284 alpha:1.00];
    [self.videoProgressSlider addSubview:smallSp];
    
    [self.darkerVideoProgressTrack addSubview:self.videoProgressSlider];
    
    
    
    self.carousel = [[iCarousel alloc] init];
    self.carousel.frame = CGRectMake(0.0, 194.0, self.view.bounds.size.width, 300.0);
    self.carousel.delegate = self;
    self.carousel.dataSource = self;
    self.carousel.type = 10;
    self.carousel.pagingEnabled = YES;
    [self.view addSubview:self.carousel];
    
    [self setupVideoControls];
}

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return (NSInteger)[self.playListItems count];
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UIImageView *videoArt = nil;
    UILabel *songTitle = nil;
    UILabel *channelName = nil;

    //create new view if no view is available for recycling
    if (view == nil)
    {
        float height = (self.view.bounds.size.width - 32.0) * 9.0 / 16.0;
        
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width - 32.0, height)];
        view.backgroundColor = [UIColor clearColor];
        view.layer.shadowColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:1.0].CGColor;
        view.layer.shadowOffset = CGSizeMake(0.0, 2.0);
        view.layer.shadowRadius = 2.0;
        view.layer.shadowOpacity = 0.5;
        
        if (index == 0) {
            
            videoArt = [UIImageView new];
            videoArt.tag = 1;
            videoArt.frame = view.bounds;
            videoArt.contentMode = UIViewContentModeScaleAspectFill;
            videoArt.clipsToBounds = YES;
            [view addSubview:videoArt];
            
            songTitle = [UILabel new];
            songTitle.tag = 2;
            songTitle.frame = CGRectMake(0.0, view.bounds.size.height - 40.0, view.bounds.size.width, 20.0);
            songTitle.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:0.5];
            songTitle.textColor = [UIColor whiteColor];
            songTitle.font = [UIFont fontWithName:@"AvenirNext-Regular" size:12.0];
            [songTitle sizeToFit];
            songTitle.textAlignment = NSTextAlignmentCenter;
            [view addSubview:songTitle];
            
            channelName = [UILabel new];
            channelName.tag = 3;
            channelName.frame = CGRectMake(0.0, view.bounds.size.height - 20.0, view.bounds.size.width, 20.0);
            channelName.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:0.5];
            channelName.textColor = [UIColor whiteColor];
            channelName.font = [UIFont fontWithName:@"AvenirNext-Regular" size:10.0];
            [channelName sizeToFit];
            channelName.textAlignment = NSTextAlignmentCenter;
            [view addSubview:channelName];
            
            self.playerView.frame = view.bounds;
            self.playerView.tag = 23;
            [view addSubview:self.playerView];
        }else {
            videoArt = [UIImageView new];
            videoArt.tag = 1;
            videoArt.frame = view.bounds;
            videoArt.contentMode = UIViewContentModeScaleAspectFill;
            videoArt.clipsToBounds = YES;
            [view addSubview:videoArt];
            
            songTitle = [UILabel new];
            songTitle.tag = 2;
            songTitle.frame = CGRectMake(0.0, view.bounds.size.height - 40.0, view.bounds.size.width, 20.0);
            songTitle.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:0.5];
            songTitle.textColor = [UIColor whiteColor];
            songTitle.font = [UIFont fontWithName:@"AvenirNext-Regular" size:12.0];
//            [songTitle sizeToFit];
            songTitle.textAlignment = NSTextAlignmentCenter;
            [view addSubview:songTitle];
            
            channelName = [UILabel new];
            channelName.tag = 3;
            channelName.frame = CGRectMake(0.0, view.bounds.size.height - 20.0, view.bounds.size.width, 20.0);
            channelName.backgroundColor = [UIColor colorWithWhite:30.0 / 255.0 alpha:0.5];
            channelName.textColor = [UIColor whiteColor];
            channelName.font = [UIFont fontWithName:@"AvenirNext-Regular" size:10.0];
//            [channelName sizeToFit];
            channelName.textAlignment = NSTextAlignmentCenter;
            [view addSubview:channelName];
        }
        
        
    }
    else
    {
        if (index == 0) {
            videoArt = (UIImageView *)[view viewWithTag:1];
            songTitle = (UILabel *)[view viewWithTag:2];
            channelName = (UILabel *)[view viewWithTag:3];
        }else {
            //get a reference to the label in the recycled view
            videoArt = (UIImageView *)[view viewWithTag:1];
            songTitle = (UILabel *)[view viewWithTag:2];
            channelName = (UILabel *)[view viewWithTag:3];
        }
        
        
    }
    if (index == 0) {
        [view addSubview:self.playerView];
    }else {
        [[view viewWithTag:23] removeFromSuperview];
        YTObject *object = (YTObject *)[self.playListItems objectAtIndex:index];
        
        NSArray *storedThumbs = [NSArray arrayWithArray:[[LocalStorage storage] thumbObjects]];
        
        BOOL stored = NO;
        for (ThumbObject *thumb in storedThumbs) {
            if ([thumb.thumbnailUrl isEqualToString:object.thumbNailUrl]) {
                videoArt.image = thumb.thumbnailImage;
                stored = YES;
                break;
            }
        }
        NSLog(@"TITLE: %@", object.videoTitle);
        songTitle.text = object.videoTitle;
        channelName.text = object.channelTitle;
        
        
        
        if (!stored) {
            dispatch_queue_t aQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(aQueue, ^{
                
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:object.thumbNailUrl]]];
                
                [[LocalStorage storage] saveThumbObject:[ThumbObject ThumbObjectWithThumbnailUrl:object.thumbNailUrl thumbnailImage:image]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    videoArt.image = image;
                    [view bringSubviewToFront:songTitle];
                    [view bringSubviewToFront:channelName];
                });
                
            });
            
        }
        
    }
    
    return view;
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 0.55f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSNumber *item = (self.playListItems)[(NSUInteger)index];
    NSLog(@"Tapped view number: %@", item);
    
    int valSelected = (int)index;
    
    NSMutableArray *mArray = [NSMutableArray new];
    for (int i = valSelected; i < self.playListItems.count; i++) {
        [mArray addObject:self.playListItems[i]];
    }
    for (int i = 0; i < valSelected; i++) {
        [mArray addObject:self.playListItems[i]];
    }
    
    PLObject *pl = [PLObject PLObjectWithPlaylistId:nil playlistName:nil videos:[NSArray arrayWithArray:mArray]];
    YTObject *video = (YTObject *)[self.playListItems objectAtIndex:index];
    
    [[CurrentPlaylist playlist] playVideo:video fromPlaylist:pl];
    
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
//    NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
}

- (void)setupVideoControls
{
    self.playPauseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.playPauseButton.frame = CGRectMake(self.view.bounds.size.width / 2.0 - 30.0, self.view.bounds.size.height - 200.0, 60.0, 60.0);

    [self.playPauseButton setImage:[[UIImage imageNamed:@"smallPlay"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.playPauseButton.imageView setTintColor:[UIColor whiteColor]];
    [self.playPauseButton setTintColor:[UIColor whiteColor]];
    
    self.playPauseButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2].CGColor;
    self.playPauseButton.layer.borderWidth = 1.0;
    self.playPauseButton.layer.cornerRadius = 30.0;
    [self.playPauseButton addTarget:self action:@selector(playPauseButtonToggle) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.playPauseButton];
    
    self.nextVideoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.nextVideoButton.frame = CGRectMake(self.view.bounds.size.width / 2.0 + 60.0, self.view.bounds.size.height - 195.0, 50.0, 50.0);
    [self.nextVideoButton setImage:[[UIImage imageNamed:@"rightArrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.nextVideoButton.imageView setTintColor:[UIColor whiteColor]];
    [self.nextVideoButton setTintColor:[UIColor whiteColor]];
    
    self.nextVideoButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2].CGColor;
    self.nextVideoButton.layer.borderWidth = 1.0;
    self.nextVideoButton.layer.cornerRadius = 25.0;
    [self.nextVideoButton addTarget:self action:@selector(moveToNextVideo) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.nextVideoButton];
    
    self.previousVideoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.previousVideoButton.frame = CGRectMake(self.view.bounds.size.width / 2.0 - 110.0, self.view.bounds.size.height - 195.0, 50.0, 50.0);
    [self.previousVideoButton setImage:[[UIImage imageNamed:@"leftArrow"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.previousVideoButton.imageView setTintColor:[UIColor whiteColor]];
    [self.previousVideoButton setTintColor:[UIColor whiteColor]];
    
    self.previousVideoButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2].CGColor;
    self.previousVideoButton.layer.borderWidth = 1.0;
    self.previousVideoButton.layer.cornerRadius = 25.0;
    [self.previousVideoButton addTarget:self action:@selector(moveToPreviousVideo) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.previousVideoButton];
    
    self.repeatButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.repeatButton.frame = CGRectMake(self.view.bounds.size.width / 2.0 + 25.0, self.view.bounds.size.height - 125.0, 40.0, 40.0);
    [self.repeatButton setImage:[[UIImage imageNamed:@"repeat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.repeatButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
    [self.repeatButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
    
    self.repeatButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2].CGColor;
    self.repeatButton.layer.borderWidth = 1.0;
    self.repeatButton.layer.cornerRadius = 20.0;
    [self.repeatButton addTarget:self action:@selector(repeatButtonToggle) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.repeatButton];
    
    self.playListButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.playListButton.frame = CGRectMake(self.view.bounds.size.width / 2.0 + 105.0, self.view.bounds.size.height - 127.5, 45.0, 45.0);
    [self.playListButton setImage:[[UIImage imageNamed:@"openHeart2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
    [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
    [self.playListButton addTarget:self action:@selector(addToPlaylistSelected) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.playListButton];
    
    
//    self.repeatText = [UILabel new];
//    self.repeatText.frame = CGRectMake(self.view.bounds.size.width / 2.0 + 50.0, self.view.bounds.size.height - 128.0, 12.0, 12.0);
//    self.repeatText.backgroundColor = [UIColor colorWithRed:0.298 green:0.804 blue:0.384 alpha:1.00];
//    self.repeatText.font = [UIFont fontWithName:@"AvenirNext-Medium" size:9.0];
//    self.repeatText.textColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0];
//    self.repeatText.text = @"1";
//    self.repeatText.layer.cornerRadius = 6.0;
//    self.repeatText.textAlignment = NSTextAlignmentCenter;
//    self.repeatText.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.repeatText.layer.borderWidth = 1.0;
//    self.repeatText.clipsToBounds = YES;
//    [self.view addSubview:self.repeatText];
    
    MPRemoteCommandCenter *rcc = [MPRemoteCommandCenter sharedCommandCenter];
    MPRemoteCommand *nextTrackCommand = [rcc nextTrackCommand];
    [nextTrackCommand setEnabled:YES];
    [nextTrackCommand addTarget:self action:@selector(moveToNextVideo)];
    
    MPRemoteCommand *previousTrackCommand = [rcc previousTrackCommand];
    [previousTrackCommand setEnabled:YES];
    [previousTrackCommand addTarget:self action:@selector(moveToPreviousVideo)];
}

- (void)playVideoFromNotification:(NSNotification *)notification
{
    NSDictionary *objects = [NSDictionary dictionaryWithDictionary:notification.userInfo];
    
    PLObject *currentPlaylist = [objects objectForKey:@"playlist"];
    
    self.playListItems = [NSArray arrayWithArray:currentPlaylist.videos];
    [self.carousel reloadData];
    
    
    
    [self.carousel scrollToItemAtIndex:[self.playListItems indexOfObject:[objects objectForKey:@"video"]] animated:YES];
    
    YTObject *currentVideo = [objects objectForKey:@"video"];
    
    BOOL isOnPlaylist = NO;
    
    NSArray *playlists = [[LocalStorage storage] loadPlaylists];
    
    for (PLObject *play in playlists) {
        for (YTObject *video in play.videos) {
            if ([video.videoId isEqualToString:currentVideo.videoId]) {
                isOnPlaylist = YES;
                break;
            }
        }
    }
    
    if (isOnPlaylist) {
        [self.playListButton setImage:[[UIImage imageNamed:@"openHeart2filled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
    }else {
        [self.playListButton setImage:[[UIImage imageNamed:@"openHeart2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
        [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
        [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
    }
    
    self.currentVideo = currentVideo;
    
    self.currentSongTitle.text = currentVideo.videoTitle;
    self.currentSongChannel.text = currentVideo.channelTitle;
    
    self.actualVideoProgressTrack.frame = CGRectMake(0.0, 0.0, 0.0, 10.0);
    self.videoProgressSlider.frame = CGRectMake(0.0, 0.0, 6.0, 10.0);
    
    self.totalSongSeconds = (self.currentVideo.hours.intValue * 60 * 60) + (self.currentVideo.minutes.intValue * 60) + self.currentVideo.seconds.intValue;
    
    
    NSString *videoId = currentVideo.videoId;
    
    // For a full list of player parameters, see the documentation for the HTML5 player
    // at: https://developers.google.com/youtube/player_parameters?playerVersion=HTML5
    NSDictionary *playerVars = @{
                                 @"controls" : @0,
                                 @"origin" : @"https://www.youtube.com",
                                 @"playsinline" : @1,
                                 @"autohide" : @1,
                                 @"showinfo" : @0,
                                 @"modestbranding" : @1,
                                 @"autoplay" : @1
                                 };
    
    [self.playerView loadWithVideoId:videoId playerVars:playerVars];
    
    [self.playerView cuePlaylistByVideos:currentPlaylist.videos index:1 startSeconds:0.0 suggestedQuality:kYTPlaybackQualityHD1080];
    
    NSLog(@"called");
}

- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView
{
    
    
    [playerView playVideo];
    self.isPlaying = YES;
    [[SinglePlayer player] playerIsPlaying:YES];
}



- (void)playerView:(YTPlayerView *)playerView didPlayTime:(float)playTime
{
    float currentProgressWidth = playTime * self.darkerVideoProgressTrack.frame.size.width / self.totalSongSeconds;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.actualVideoProgressTrack.frame = CGRectMake(0.0, 0.0, currentProgressWidth, 10.0);
        self.videoProgressSlider.frame = CGRectMake(currentProgressWidth, 0.0, 6.0, 10.0);
    }];
    
    if (self.totalSongSeconds - playTime < 6 && self.repeatSongEnabled) {
        [self.playerView seekToSeconds:2.0 allowSeekAhead:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (self.currentVideo != nil) {
        // Set title and channel and button
        if (self.isPlaying) {
            [self.playPauseButton setImage:[UIImage imageNamed:@"pause2"] forState:UIControlStateNormal];
        }else {
            [self.playPauseButton setImage:[UIImage imageNamed:@"smallPlay"] forState:UIControlStateNormal];
        }
        
        
        self.currentSongTitle.text = self.currentVideo.videoTitle;
        self.currentSongChannel.text = self.currentVideo.channelTitle;
        
        float currentProgressWidth = self.playerView.currentTime * self.darkerVideoProgressTrack.frame.size.width / self.totalSongSeconds;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.actualVideoProgressTrack.frame = CGRectMake(0.0, 0.0, currentProgressWidth, 10.0);
            self.videoProgressSlider.frame = CGRectMake(currentProgressWidth, 0.0, 6.0, 10.0);
        }];
        
        BOOL isOnPlaylist = NO;
        
        NSArray *playlists = [[LocalStorage storage] loadPlaylists];
        
        for (PLObject *play in playlists) {
            for (YTObject *video in play.videos) {
                if ([video.videoId isEqualToString:self.currentVideo.videoId]) {
                    isOnPlaylist = YES;
                    break;
                }
            }
        }
        
        if (isOnPlaylist) {
            [self.playListButton setImage:[[UIImage imageNamed:@"openHeart2filled"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
            [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        }else {
            [self.playListButton setImage:[[UIImage imageNamed:@"openHeart2"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
            [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
        }
    }
}

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state
{
    if (state == kYTPlayerStatePaused) {
//        if ([[UIApplication sharedApplication] applicationState] == UIApplicationStateInactive) {
//            [self.playerView playVideo];
//        }else {
            self.isPlaying = NO;
        
            [self.playPauseButton setImage:[UIImage imageNamed:@"smallPlay"] forState:UIControlStateNormal];
//        }
    }else if (state == kYTPlayerStatePlaying) {
        self.isPlaying = YES;
        [self.playPauseButton setImage:[UIImage imageNamed:@"pause2"] forState:UIControlStateNormal];
    }else if (state == kYTPlayerStateEnded) {
        [[CurrentPlaylist playlist] playNextVideo];
    }
}

- (void)playPauseButtonToggle
{
    if (self.isPlaying) {
        self.isPlaying = NO;
        [[SinglePlayer player] playerIsPlaying:NO];
        [self.playPauseButton setImage:[UIImage imageNamed:@"smallPlay"] forState:UIControlStateNormal];
        [self.playerView pauseVideo];
    }else {
        self.isPlaying = YES;
        [[SinglePlayer player] playerIsPlaying:YES];
        [self.playPauseButton setImage:[UIImage imageNamed:@"pause2"] forState:UIControlStateNormal];
        [self.playerView playVideo];
    }
}

- (void)repeatButtonToggle
{
    if (self.repeatSongEnabled) {
        self.repeatSongEnabled = NO;
        [self.repeatButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
        [self.repeatButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:0.2]];
        
        self.repeatButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.2].CGColor;
    }else {
        self.repeatSongEnabled = YES;
        [self.repeatButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        [self.repeatButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
        
        self.repeatButton.layer.borderColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:1.0].CGColor;
    }
}

- (void)moveToNextVideo
{
    [[CurrentPlaylist playlist] playNextVideo];
}

- (void)moveToPreviousVideo
{
    [[CurrentPlaylist playlist] playPreviousVideo];
}

- (void)addToPlaylistSelected
{
    if (self.songIsOnPlaylist) {
        
    }else {
        // Present to add to playlist.
        ((BaseViewController *)self.tabBarController).addingToPlaylistDelegate = self;
        [((BaseViewController *)self.tabBarController) showVideoOptionsForYTObject:self.currentVideo];
        
    }
}

- (void)didAddVideoToPlaylist
{
    [[LocalStorage storage] updatePlaylists];
    
    [self.playListButton setImage:[UIImage imageNamed:@"openHeart2filled"] forState:UIControlStateNormal];
    [self.playListButton.imageView setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
    [self.playListButton setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
}



@end
