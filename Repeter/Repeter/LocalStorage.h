//
//  LocalStorage.h
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThumbObject.h"

@protocol PlaylistsUpdatedDelegate <NSObject>

- (void)playlistsUpdated;

@end

@interface LocalStorage : NSObject

+ (LocalStorage *)storage;

@property (nonatomic, assign) id<PlaylistsUpdatedDelegate>playlistLocalStorageDelegate;

- (NSArray *)thumbObjects;

- (void)saveThumbObject:(ThumbObject *)object;

- (void)savePlaylists:(NSArray *)playlists;

- (NSArray *)loadPlaylists;

- (void)updatePlaylists;


- (void)saveLastSearch:(NSArray *)lastSearch;

- (NSArray *)loadLastSearch;

- (void)saveLastSearchTerm:(NSString *)lastSearchTerm;

- (NSString *)loadLastSearchTerm;

@end
