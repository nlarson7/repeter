//
//  LaunchS1.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "LaunchS1.h"

#import "BaseViewController.h"
#import "Browse.h"
#import "Playlists.h"
#import "Playing.h"
#import "Discover.h"
#import "Profile.h"
#import "Server.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <TwitterKit/TwitterKit.h>


@interface LaunchS1 ()<ServerCreateUserDelegate>

@property (nonatomic) UIPageControl *pageControl;
@property (nonatomic) UIView *pageControlAmplifier;

@end

@implementation LaunchS1

- (void)viewDidLoad
{
    self.navigationController.navigationBarHidden = YES;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    [self.view.layer insertSublayer:gradient atIndex:0];
    
    [self setupImage];
    [self setupIcon];
    [self setupLowerSection];
    [self setupPageControl];
}

- (void)setupImage
{
    UIImageView *imageView = [UIImageView new];
    imageView.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, self.view.bounds.size.height);
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.image = [UIImage imageNamed:@"concert"];
    imageView.alpha = 0.4;
    [self.view addSubview:imageView];
}

- (void)setupIcon
{
    UIImageView *imageView = [UIImageView new];
    imageView.frame = CGRectMake(self.view.bounds.size.width / 2.0 - 130.0, self.view.bounds.size.height / 2.0 - 260.0, 260.0, 260.0);
    imageView.image = [UIImage imageNamed:@"MainIcon"];
    [self.view addSubview:imageView];
    
    UILabel *subTitle = [UILabel new];
    subTitle.frame = CGRectMake(25.0, self.view.bounds.size.height / 2.0 - 35.0, self.view.bounds.size.width - 32.0, 22.0);
    subTitle.textColor = [UIColor whiteColor];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"REPETER"];
    [attributedString addAttribute:NSKernAttributeName
                             value:@(6.5)
                             range:NSMakeRange(0, 7)];
    
    subTitle.attributedText = attributedString;
    subTitle.font = [UIFont fontWithName:@"AvenirNext-Bold" size:18.0];
    subTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:subTitle];
}

- (void)setupLowerSection
{
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(0.0, self.view.bounds.size.height - 120.0, self.view.bounds.size.width, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.3];
    [self.view addSubview:seperatorLine];
    
    UIButton *facebookB = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    facebookB.frame = CGRectMake(72.0, self.view.bounds.size.height - 104.0, self.view.bounds.size.width - 144.0, 50.0);
    facebookB.backgroundColor = [UIColor colorWithRed:75.0 / 255.0 green:142.0 / 255.0 blue:255.0 / 255.0 alpha:1.0];
    facebookB.layer.cornerRadius = 3.0;
    [facebookB.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Bold" size:18.0]];
    [facebookB setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [facebookB setTitle:@"Enter" forState:UIControlStateNormal];
    [facebookB addTarget:self action:@selector(facebookButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:facebookB];
    
    UIButton *twitterB = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    twitterB.frame = CGRectMake(self.view.bounds.size.width / 2.0 + 16.0, self.view.bounds.size.height - 104.0, self.view.bounds.size.width / 2.0 - 32.0, 50.0);
    twitterB.backgroundColor = [UIColor colorWithRed:3.0 / 255.0 green:203.0 / 255.0 blue:248.0 / 255.0 alpha:1.0];
    twitterB.layer.cornerRadius = 3.0;
    [twitterB.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Bold" size:18.0]];
    [twitterB setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [twitterB setTitle:@"t" forState:UIControlStateNormal];
    [twitterB addTarget:self action:@selector(twitterButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:twitterB];
    
    UILabel *explanation = [UILabel new];
    explanation.frame = CGRectMake(16.0, self.view.bounds.size.height - 44.0, self.view.bounds.size.width - 32.0, 34.0);
    explanation.textAlignment = NSTextAlignmentCenter;
    explanation.textColor = [UIColor whiteColor];
    explanation.text = @"";
    explanation.font = [UIFont fontWithName:@"AvenirNext-Regular" size:12.0];
    [self.view addSubview:explanation];
}

- (void)setupPageControl
{
    self.pageControl = [UIPageControl new];
    self.pageControl.frame = CGRectMake(16.0, self.view.bounds.size.height - 184.0, self.view.bounds.size.width - 32.0, 44.0);
    self.pageControl.tintColor = [UIColor whiteColor];
    self.pageControl.numberOfPages = 3;
//    [self.view addSubview:self.pageControl];
    
    
}

- (void)facebookButtonPressed
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"] intValue] > 0) {
        [self userCreated];
    }else {
        
        [Server API].serverCreateUserDelegate = self;
        [[Server API] sendData:@{@"fb_id":[[NSUUID UUID] UUIDString], @"email":@""} toEndPoint:@"CREATE_USER"];
        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Facebook" message:@"We request your public profile and email address so that we can keep your playlists in sync across your devices." preferredStyle:UIAlertControllerStyleAlert];
//        
//        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//            
//        }]];
//        
//        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//            
//            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
//            [login
//             logInWithReadPermissions: @[@"public_profile", @"email"]
//             fromViewController:self
//             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//                 if (error) {
//                     NSLog(@"Process error");
//                 } else if (result.isCancelled) {
//                     NSLog(@"Cancelled");
//                 } else {
//                     
//                     NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
//                     [parameters setValue:@"id,name,email" forKey:@"fields"];
//                     
//                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
//                      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
//                                                   id result, NSError *error) {
//                          
//                          [Server API].serverCreateUserDelegate = self;
//                          [[Server API] sendData:@{@"fb_id":[result valueForKey:@"id"], @"email":@""} toEndPoint:@"CREATE_USER"];
//                      }];
//                 }
//             }];
//        }]];
//        
//        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

- (void)twitterButtonPressed
{
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"user_id"] intValue] > 0) {
        [self userCreated];
    }else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Facebook" message:@"We request your public profile and email address so that we can keep your playlists in sync across your devices." preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
        }]];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                if (session) {
                    NSLog(@"signed in as %@", [session userName]);
                    
                    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
                    NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                                     URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                              parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                                   error:nil];
                    
                    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                        
                        NSArray *arrayData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                        
                        NSLog(@"%@", arrayData);
                        
                    }];
                    
                } else {
                    NSLog(@"error: %@", [error localizedDescription]);
                }
            }];
        
        }]];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    
}

- (void)userCreated
{
    Browse *browse = [Browse new];
    UINavigationController *bNav = [[UINavigationController alloc] initWithRootViewController:browse];
    
//    Discover *discover = [Discover new];
    Playing *playing = [Playing new];
    UINavigationController *playingNav = [[UINavigationController alloc] initWithRootViewController:playing];
    
    Playlists *playlists = [Playlists new];
    UINavigationController *playlistNav = [[UINavigationController alloc] initWithRootViewController:playlists];
    
//    Profile *profile = [Profile new];
    
    BaseViewController *tabbarController = [BaseViewController new];
    tabbarController.viewControllers = @[bNav, playingNav, playlistNav];
    
    [tabbarController addCenterButtonWithImage:nil highlightImage:nil];
    
    [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithWhite:255.0 / 255.0 alpha:1.0]];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{
                                                        NSFontAttributeName:[UIFont fontWithName:@"AvenirNext-Medium" size:8.0f]
                                                        } forState:UIControlStateNormal];
    
    [self.navigationController pushViewController:tabbarController animated:YES];
}

@end
