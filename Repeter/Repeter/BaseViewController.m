//
//  BaseViewController.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "BaseViewController.h"
#import "Server.h"
#import "PLObject.h"

@interface BaseViewController ()<ServerLoadPlaylists, ServerAddedVideoToPlaylistDelegate>

@property (nonatomic) UIVisualEffectView *videoOptionsView;
@property (nonatomic) UIVisualEffectView *loadingView;
@property (nonatomic) UILabel *stateLabel;
@property (nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) BOOL loadingViewShowing;
@property (nonatomic) YTObject *selectedObject;

@property (nonatomic) UIVisualEffectView *playlistOptionsView;
@property (nonatomic) UIScrollView *playlistScrollView;

@property (nonatomic) NSArray *localPlaylists;

@end

@implementation BaseViewController

-(void)addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, 60.0, 60.0);
    button.layer.cornerRadius = 30.0;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.layer.borderWidth = 1.0;
    button.layer.shadowColor = [[UIColor blackColor] CGColor];
    button.layer.shadowOffset = CGSizeMake(0,3);
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowRadius = 3.0;
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = button.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:170.0 / 255.0 green:48.0 / 255.0 blue:216.0 / 255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] CGColor], nil];
    gradient.cornerRadius = 30.0;
    [button.layer insertSublayer:gradient atIndex:0];
    
    [button setImage:[UIImage imageNamed:@"logo"] forState:UIControlStateNormal];
    //    [button setTitle:@"+" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-UltraLight" size:46.0]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.247 green:0.337 blue:0.612 alpha:1.00];
    [button addTarget:self action:@selector(pressedButton) forControlEvents:UIControlEventTouchUpInside];
    //    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    [button bringSubviewToFront:button.imageView];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        button.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    
    button.center = CGPointMake(button.center.x, button.center.y - 5.0);
    
//    UIView *topPart = [UIView new];
//    topPart.frame = CGRectMake(0.0, self.view.bounds.size.height - 50.0, self.view.bounds.size.width, 1.0);
//    topPart.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:topPart];
    
    [self.view addSubview:button];
    
    
    
    [self setupVideoOptions];
    [self setupLoadingScreen];
    [self setupPlaylistOptions];
}

- (void)pressedButton
{
    [self setSelectedIndex:1];
}

- (void)setupLoadingScreen
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    self.loadingView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    self.loadingView.alpha = 0.0;
    self.loadingView.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, self.view.bounds.size.height);
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.frame = CGRectMake(self.view.bounds.size.width / 2.0 - 22.0, self.view.bounds.size.height / 2.0 - 44.0, 44.0, 44.0);
    [self.loadingView addSubview:self.activityIndicator];
    
    self.stateLabel = [UILabel new];
    self.stateLabel.frame = CGRectMake(16.0, self.view.bounds.size.height / 2.0, self.view.bounds.size.width - 32.0, 44.0);
    self.stateLabel.textColor = [UIColor whiteColor];
    self.stateLabel.textAlignment = NSTextAlignmentCenter;
    self.stateLabel.font = [UIFont fontWithName:@"AvenirNext-UltraLight" size:18.0];
    self.stateLabel.text = @"Loading Playlists..";
    [self.loadingView addSubview:self.stateLabel];
    
    [self.view addSubview:self.loadingView];
}

-(void)showLoadingViewWithText:(NSString *)text
{
    self.stateLabel.text = text;
    [self.activityIndicator startAnimating];
    [UIView animateWithDuration:0.3 animations:^{
        self.loadingView.alpha = 1.0;
    }];
    self.loadingViewShowing = YES;
}

- (BOOL)isShowingLoadingView
{
    return self.loadingViewShowing;
}

- (void)hideLoadingView
{
    [UIView animateWithDuration:0.3 animations:^{
        self.loadingView.alpha = 0.0;
    }completion:^(BOOL finished){
        [self.activityIndicator stopAnimating];
        self.loadingViewShowing = NO;
    }];
}

- (void)setupVideoOptions
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    self.videoOptionsView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    self.videoOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    
    
    [self.view addSubview:self.videoOptionsView];
    
    UIButton *addToPlaylistButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    addToPlaylistButton.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 80.0);
    [addToPlaylistButton.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18.0]];
    [addToPlaylistButton setTitle:@"Add to Playlist" forState:UIControlStateNormal];
    [addToPlaylistButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addToPlaylistButton addTarget:self action:@selector(showPlaylists) forControlEvents:UIControlEventTouchUpInside];
    [self.videoOptionsView addSubview:addToPlaylistButton];
    
    UIView *seperatorLine1 = [UIView new];
    seperatorLine1.frame = CGRectMake(16.0, 80.0, self.view.bounds.size.width - 32.0, 0.5);
    seperatorLine1.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
    [self.videoOptionsView addSubview:seperatorLine1];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelButton.frame = CGRectMake(0.0, 80.5, self.view.bounds.size.width, 80.0);
    [cancelButton.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18.0]];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelVideoOptions) forControlEvents:UIControlEventTouchUpInside];
    [self.videoOptionsView addSubview:cancelButton];
    
    UIView *seperatorLine2 = [UIView new];
    seperatorLine2.frame = CGRectMake(16.0, 160.5, self.view.bounds.size.width - 32.0, 0.5);
    seperatorLine2.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
    [self.videoOptionsView addSubview:seperatorLine2];
}

- (void)setupPlaylistOptions
{
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    self.playlistOptionsView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    
    [self.view addSubview:self.playlistOptionsView];
    
    self.playlistScrollView = [UIScrollView new];
    
    [self.playlistOptionsView addSubview:self.playlistScrollView];
    
    // Will have a scrollview.
    // Cancel will be at the bottom.
    // Will get bigger until size exceeds height.
}

- (void)showVideoOptionsForYTObject:(YTObject *)object
{
    self.selectedObject = object;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.videoOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height - 161.0, self.view.bounds.size.width, self.view.bounds.size.height);
    }];
}

- (void)cancelVideoOptions
{
    [UIView animateWithDuration:0.3 animations:^{
        self.videoOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    }];
}

- (void)showPlaylists
{
    [self showLoadingViewWithText:@"Loading Playlists.."];
    [Server API].serverLoadedPlaylistsDelegate = self;
    [[Server API] sendData:nil toEndPoint:@"LOAD_PLAYLISTS"];
}

- (void)loadedPlaylists:(NSArray *)playlists
{
    // Figure out the height of the scrollview.
    // Each piece is 60 pts heigh
    
    self.localPlaylists = [NSArray arrayWithArray:playlists];
    
    float height = 80.0 * playlists.count;
    height = height + 80.0;
    
    for (UIView *aView in self.playlistScrollView.subviews) {
        [aView removeFromSuperview];
    }
    
    if (height > 320.0) {
        self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, 320.0);
        self.playlistScrollView.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, 240.0);
        self.playlistScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, height - 80.0);
    }else {
        self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, height);
        self.playlistScrollView.frame = CGRectMake(0.0, 0.0, self.view.bounds.size.width, height - 80.0);
        self.playlistScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, height - 80.0);
    }
    
    
    
    float yPos = 0.0;
    float yPos2 = 80.0;
    
    
    
    for (PLObject *object in playlists) {
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(0.0, yPos, self.view.bounds.size.width, 80.0);
        [button.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18.0]];
        [button setTitle:object.playlistName forState:UIControlStateNormal];
        [button addTarget:self action:@selector(playlistSelected:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.playlistScrollView addSubview:button];
        
        UIView *seperatorLine = [UIView new];
        seperatorLine.frame = CGRectMake(16.0, yPos2, self.view.bounds.size.width - 32.0, 0.5);
        seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
        [self.playlistScrollView addSubview:seperatorLine];
        
        yPos = yPos + 80.5;
        yPos2 = yPos2 + 80.5;
        
    }
    
    [[self.playlistOptionsView viewWithTag:111] removeFromSuperview];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    cancelButton.tag = 111;
    if (height > 320.0) {
        cancelButton.frame = CGRectMake(0.0, 240.0, self.view.bounds.size.width, 80.0);
    }else {
        cancelButton.frame = CGRectMake(0.0, yPos, self.view.bounds.size.width, 80.0);
    }
    
    
    [cancelButton.titleLabel setFont:[UIFont fontWithName:@"AvenirNext-Medium" size:18.0]];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithRed:130.0 / 255.0 green:69.0 / 255.0 blue:251.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelPlaylistOptions) forControlEvents:UIControlEventTouchUpInside];
    [self.playlistOptionsView addSubview:cancelButton];
    
    UIView *seperatorLine = [UIView new];
    seperatorLine.frame = CGRectMake(16.0, yPos2, self.view.bounds.size.width - 32.0, 0.5);
    seperatorLine.backgroundColor = [UIColor colorWithWhite:255.0 / 255.0 alpha:0.1];
    [self.playlistOptionsView addSubview:seperatorLine];
    
    
    [self hideLoadingView];
    
    [UIView animateWithDuration:0.3 animations:^{
        if (height > 320) {
            self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height - 320.0, self.view.bounds.size.width, 320.0);
        }else {
            self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height - height, self.view.bounds.size.width, height);
        }
        
        self.videoOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    }];
    
}

- (void)cancelPlaylistOptions
{
    [UIView animateWithDuration:0.3 animations:^{
        self.playlistOptionsView.frame = CGRectMake(0.0, self.view.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height);
    }];
}

- (void)playlistSelected:(UIButton *)sender
{
    [self showLoadingViewWithText:@"Adding to Playlist.."];
    [Server API].serverAddedVideoToPlaylistDelegate = self;
    
    NSString *playlist_id = nil;
    for (PLObject *object in self.localPlaylists) {
        if ([object.playlistName isEqualToString:sender.titleLabel.text]) {
            playlist_id = object.playlistId;
        }
    }
    
    [self cancelPlaylistOptions];
    
    [[Server API] sendData:@{@"video_id":self.selectedObject.videoId
                             , @"video_title":self.selectedObject.videoTitle
                             , @"video_description":self.selectedObject.videoDescription
                             , @"channel_title":self.selectedObject.channelTitle
                             , @"view_count":self.selectedObject.viewCount
                             , @"thumbnail_url":self.selectedObject.thumbNailUrl
                             , @"playlist_id":playlist_id
                             , @"hours":self.selectedObject.hours
                             , @"minutes":self.selectedObject.minutes
                             , @"seconds":self.selectedObject.seconds
                             } toEndPoint:@"ADD_TO_PLAYLIST"];
}

- (void)videoAddedToPlaylist
{
    [self hideLoadingView];
}

@end
