//
//  SinglePlayer.h
//  Repeter
//
//  Created by Nathan Larson on 7/3/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTPlayerView.h"

@interface SinglePlayer : NSObject

+ (SinglePlayer *)player;

- (YTPlayerView *)getCurrentPlayerView;

- (void)playerIsPlaying:(BOOL)playing;
- (void)continuePlayingVideo;
- (void)pausePlayingVideo;
- (BOOL)isPlayerPlayingVideo;

@end
