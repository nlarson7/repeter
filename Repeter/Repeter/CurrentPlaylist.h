//
//  CurrentPlaylist.h
//  Repeter
//
//  Created by Nathan Larson on 6/30/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PLObject.h"
#import "YTObject.h"

@interface CurrentPlaylist : NSObject

+ (CurrentPlaylist *)playlist;

- (void)playVideo:(YTObject *)video fromPlaylist:(PLObject *)playlist_object;

- (void)playNextVideo;

- (void)playPreviousVideo;

@end
