//
//  YTObject.m
//  Repeter
//
//  Created by Nathan Larson on 6/28/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "YTObject.h"

@implementation YTObject

- (id)initWithVideoId:(NSString *)videoId videoTitle:(NSString *)videoTitle videoDescription:(NSString *)videoDescription channelTitle:(NSString *)channelTitle viewCount:(NSString *)viewCount thumbNailUrl:(NSString *)thumbNailUrl playlistId:(NSString *)playlistId hours:(NSString *)hours minutes:(NSString *)minutes seconds:(NSString *)seconds unique_id:(NSString *)unique_id
{
    self = [super init];
    if (self) {
        self.videoId = videoId;
        self.videoTitle = videoTitle;
        self.videoDescription = videoDescription;
        self.channelTitle = channelTitle;
        self.viewCount = viewCount;
        self.thumbNailUrl = thumbNailUrl;
        self.playlistId = playlistId;
        self.hours = hours;
        self.minutes = minutes;
        self.seconds = seconds;
        self.unique_id = unique_id;
    }
    return self;
}

+ (id)YTObjectWithVideoId:(NSString *)videoId videoTitle:(NSString *)videoTitle videoDescription:(NSString *)videoDescription channelTitle:(NSString *)channelTitle viewCount:(NSString *)viewCount thumbNailUrl:(NSString *)thumbNailUrl playlistId:(NSString *)playlistId hours:(NSString *)hours minutes:(NSString *)minutes seconds:(NSString *)seconds unique_id:(NSString *)unique_id
{
    return [[self alloc] initWithVideoId:videoId videoTitle:videoTitle videoDescription:videoDescription channelTitle:channelTitle viewCount:viewCount thumbNailUrl:thumbNailUrl playlistId:playlistId hours:hours minutes:minutes seconds:seconds unique_id:unique_id];
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.videoId forKey:@"videoId"];
    [encoder encodeObject:self.videoTitle forKey:@"videoTitle"];
    [encoder encodeObject:self.videoDescription forKey:@"videoDescription"];
    [encoder encodeObject:self.channelTitle forKey:@"channelTitle"];
    [encoder encodeObject:self.viewCount forKey:@"viewCount"];
    [encoder encodeObject:self.thumbNailUrl forKey:@"thumbNailUrl"];
    [encoder encodeObject:self.playlistId forKey:@"playlistId"];
    [encoder encodeObject:self.hours forKey:@"hours"];
    [encoder encodeObject:self.minutes forKey:@"minutes"];
    [encoder encodeObject:self.seconds forKey:@"seconds"];
    [encoder encodeObject:self.unique_id forKey:@"unique_id"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.videoId = [decoder decodeObjectForKey:@"videoId"];
        self.videoTitle = [decoder decodeObjectForKey:@"videoTitle"];
        self.videoDescription = [decoder decodeObjectForKey:@"videoDescription"];
        self.channelTitle = [decoder decodeObjectForKey:@"channelTitle"];
        self.viewCount = [decoder decodeObjectForKey:@"viewCount"];
        self.thumbNailUrl = [decoder decodeObjectForKey:@"thumbNailUrl"];
        self.playlistId = [decoder decodeObjectForKey:@"playlistId"];
        self.hours = [decoder decodeObjectForKey:@"hours"];
        self.minutes = [decoder decodeObjectForKey:@"minutes"];
        self.seconds = [decoder decodeObjectForKey:@"seconds"];
        self.unique_id = [decoder decodeObjectForKey:@"unique_id"];
    }
    return self;
}

@end
