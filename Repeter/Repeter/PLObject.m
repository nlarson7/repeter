//
//  PLObject.m
//  Repeter
//
//  Created by Nathan Larson on 6/29/16.
//  Copyright © 2016 appselevated. All rights reserved.
//

#import "PLObject.h"

@implementation PLObject

- (id)initWithPlaylistId:(NSString *)playlist_id playlistName:(NSString *)playlist_name videos:(NSArray *)videos
{
    self = [super init];
    if (self) {
        self.playlistId = playlist_id;
        self.playlistName = playlist_name;
        self.videos = [NSArray arrayWithArray:videos];
    }
    return self;
}

+ (id)PLObjectWithPlaylistId:(NSString *)playlist_id playlistName:(NSString *)playlist_name videos:(NSArray *)videos
{
    return [[self alloc] initWithPlaylistId:playlist_id playlistName:playlist_name videos:[NSArray arrayWithArray:videos]];
}

- (void)addVideo:(YTObject *)video
{
    NSMutableArray *t_vid = [NSMutableArray arrayWithArray:self.videos];
    [t_vid addObject:video];
    self.videos = [NSArray arrayWithArray:t_vid];
}

- (void)removeVideo:(YTObject *)video
{
    NSMutableArray *t_vid = [NSMutableArray arrayWithArray:self.videos];
    [t_vid removeObject:video];
    self.videos = [NSArray arrayWithArray:t_vid];
}

-(void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:self.playlistId forKey:@"playlistId"];
    [encoder encodeObject:self.playlistName forKey:@"playlistName"];
    [encoder encodeObject:self.videos forKey:@"videos"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.playlistId = [decoder decodeObjectForKey:@"playlistId"];
        self.playlistName = [decoder decodeObjectForKey:@"playlistName"];
        self.videos = [decoder decodeObjectForKey:@"videos"];
    }
    return self;
}

@end
